﻿using UnityEngine.SceneManagement;
using System.Collections.Generic;
using UnityHelpers.RayBlocker;
using System.Collections;
using UnityEngine.UI;
using UnityEngine;
using DG.Tweening;
using System;

public enum FadeMode { BlackFade, ImageFade, Mask }

namespace UnityHelpers.FadeManager
{
    public class FadeManager : Manager
    {
        public FadeManagerMask _fadeManagerMask;

        private List<string> _scenesQueue = new List<string>();

        public static string GetLastSceneQueue()
        {
            if (instance._scenesQueue.Count == 0) return string.Empty;
            return instance._scenesQueue[instance._scenesQueue.Count - 2];
        }

        public static void AddLastSceneQueue(string sceneName)
        {
            instance._scenesQueue.Add(sceneName);
        }

        public static void RemoveLastSceneQueue()
        {
            instance._scenesQueue.RemoveAt(instance._scenesQueue.Count - 1);

            if (instance._scenesQueue.Count == 1)
            {
                instance._scenesQueue = new List<string>();
            }
        }

        private static FadeManager instance;

        #region Variables

        [Header("- Fade -")]
        [SerializeField] private CanvasGroup canvasGroup;

        [Header("- Fade Manager -")]
        [SerializeField] private GameObject fadeManager;
        [SerializeField] private Image backgroundImage;

        [Header("- Mask Manager -")]
        [SerializeField] private RectTransform maskTransform;
        [SerializeField] private float maxMaskScale = 57f;
        [SerializeField] private GameObject maskManager;
        [SerializeField] private Image maskImage;

        private Coroutine loadSceneCoroutine;
        private Coroutine fadeCoroutine;
        private Tween fadeTween;

        #endregion

        #region Instance

        public override void Initialize()
        {
            if (instance == null)
            {
                instance = this;
            }
            else if (instance != this)
            {
                Destroy(gameObject);
            }

            canvasGroup.blocksRaycasts = false;
            DontDestroyOnLoad(gameObject);

            SelectMode(FadeMode.BlackFade, MaskMode.None);
        }

        #endregion

        #region LoadScene

        public static void SelectMode(FadeMode fadeMode, MaskMode maskMode)
        {
            instance.fadeManager.SetActive(false);
            instance.maskManager.SetActive(false);

            if (fadeMode == FadeMode.Mask)
            {
                instance.maskManager.SetActive(true);
                instance.backgroundImage.DOColor(new Color(0.239f, 0f, 0.674f, 1f), 0.1f);

                if (instance._fadeManagerMask)
                {
                    instance.maskImage.sprite = instance._fadeManagerMask.GetMaskImage(maskMode);
                }
            }
            else if (fadeMode == FadeMode.BlackFade)
            {
                instance.fadeManager.SetActive(true);
                instance.backgroundImage.DOColor(Color.black, 0.1f);
            }
            else if (fadeMode == FadeMode.ImageFade)
            {
                instance.fadeManager.SetActive(true);
                instance.backgroundImage.DOColor(Color.white, 0.1f);
            }
        }

        public static void FadeIn(float duration = 1f, Action OnEndFadeIn = null, bool useInPause = false, FadeMode fadeMode = FadeMode.BlackFade, MaskMode maskMode = MaskMode.None)
        {
            if (!instance) return;

            OnEndFadeIn += () =>
            {
                instance.fadeManager.SetActive(false);
                instance.maskManager.SetActive(false);
            };
        
            instance.Fade(0, duration, OnEndFadeIn, useInPause, fadeMode, maskMode);
        }

        public static void FadeOut(float duration = 1f, Action OnEndFadeOut = null, bool useInPause = false, FadeMode fadeMode = FadeMode.BlackFade, MaskMode maskMode = MaskMode.None)
        {
            if (!instance) return;

            instance.Fade(1, duration, OnEndFadeOut, useInPause, fadeMode, maskMode);
        }

        private void Fade(int value, float duration, Action OnEndFade, bool useInPause = false, FadeMode fadeMode = FadeMode.BlackFade, MaskMode maskMode = MaskMode.None)
        {
            instance.canvasGroup.blocksRaycasts = true;

            fadeCoroutine = StartCoroutine(FadeAsync(value, duration, useInPause, OnEndFade, fadeMode, maskMode));
        }

        private IEnumerator FadeAsync(int value, float duration, bool useInPause, Action OnEndFade, FadeMode fadeMode = FadeMode.BlackFade, MaskMode maskMode = MaskMode.None)
        {
            RayBlockerController.Open();

            SelectMode(fadeMode, maskMode);

            if (fadeMode == FadeMode.Mask)
            {
                canvasGroup.alpha = 1f;
                maskTransform.localScale = Vector3.one * (value * maxMaskScale);
                fadeTween = maskTransform.DOScale(Vector3.one * (Mathf.Abs(1f - value) * maxMaskScale), duration).SetEase(value == 1 ? Ease.OutExpo : Ease.InExpo).SetUpdate(useInPause);
            }
            else
            {
                // SetUpdate true serve para a animação funcionar mesmo com TimeScale 0, porém isso causa travamentos durante o fade, não ficando um fade suave. Talvez tenha como resolver.
                canvasGroup.alpha = Mathf.Abs(1f - value);
                fadeTween = canvasGroup.DOFade(value, duration).SetUpdate(useInPause);
            }

            yield return new WaitForSeconds(duration);

            if (fadeMode == FadeMode.Mask)
            {
                yield return new WaitForSeconds(0.2f);
            }

            instance.canvasGroup.blocksRaycasts = false;

            RayBlockerController.Close();

            OnEndFade?.Invoke();
        }

        #endregion

        public static void SetActiveScene(string sceneName)
        {
            Scene scene = SceneManager.GetSceneByName(sceneName);
            SceneManager.SetActiveScene(scene);
        }

        public static void SetFadeEnabledByState(bool state, FadeMode fadeMode)
        {
            instance.canvasGroup.alpha = state ? 1 : 0;
            instance.canvasGroup.blocksRaycasts = state;
            instance.maskTransform.localScale = Vector3.one * (state ? 0 : 1);

            SelectMode(fadeMode, MaskMode.None);
        }

        public static void StopFade()
        {
            if (instance.fadeCoroutine != null) instance.StopCoroutine(instance.fadeCoroutine);
            if (instance.loadSceneCoroutine != null) instance.StopCoroutine(instance.loadSceneCoroutine);
            instance.fadeTween.Kill();
        }
    }
}