﻿using System.Collections.Generic;
using UnityEngine;

namespace UnityHelpers.FadeManager
{
    public enum MaskMode { None, Mask_A, Mask_B }

    [System.Serializable]
    public struct FadeManagerMaskItem
    {
        public MaskMode maskMode;
        public Sprite sprite;
    }

    public class FadeManagerMask : MonoBehaviour
    {
        public List<FadeManagerMaskItem> _fadeManagerMaskItems = new List<FadeManagerMaskItem>();

        public Sprite GetMaskImage(MaskMode maskMode)
        {
            if(maskMode == MaskMode.None)
            {
                return null;
            }

            foreach (var fadeManagerMaskItem in _fadeManagerMaskItems)
            {
                if (fadeManagerMaskItem.maskMode == maskMode)
                {
                    return fadeManagerMaskItem.sprite;
                }
            }

            Debug.Log($"Error: O FadeManagerMaskItem '{maskMode}' não foi encontrado");
            return null;
        }
    }
}