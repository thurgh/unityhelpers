﻿using UnityHelpers.SceneController;
using System;

public class None_ScreenAnimation : BaseScreenAnimation
{
    public override void CloseWindow(Action OnComplete)
    {
        OnComplete?.Invoke();
    }

    public override void OpenWindow(Action OnComplete)
    {
        OnComplete?.Invoke();
    }
}