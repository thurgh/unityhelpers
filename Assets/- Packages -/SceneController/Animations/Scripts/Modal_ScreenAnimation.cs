﻿using UnityHelpers.SceneController;
using UnityHelpers.RayBlocker;
using UnityEngine.UI;
using DG.Tweening;
using UnityEngine;
using System;

public class Modal_ScreenAnimation : BaseScreenAnimation
{
    [SerializeField] public RectTransform modal;
    [SerializeField] public Image background;

    public override void OpenWindow(Action OnComplete = null)
    {
        RayBlockerController.Open();
        InstantCloseWindow();

        background.DOFade(0.5f, 0.5f);

        modal.DOScale(1f, 0.5f).SetEase(Ease.OutBack).OnComplete(() =>
        {
            RayBlockerController.Close();
            OnComplete?.Invoke();
        });
    }

    public override void CloseWindow(Action OnComplete = null)
    {
        RayBlockerController.Open();
        InstantOpenWindow();

        background.DOFade(0f, 0.5f);

        modal.DOScale(0f, 0.5f).SetEase(Ease.InBack).OnComplete(() =>
        {
            RayBlockerController.Close();
            OnComplete?.Invoke();
        });
    }

    public void InstantOpenWindow()
    {
        background.DOFade(0.5f, 0f);
        modal.DOScale(1f, 0f);
    }

    public void InstantCloseWindow()
    {
        background.DOFade(0f, 0f);
        modal.DOScale(0f, 0f);
    }
}