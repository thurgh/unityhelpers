using UnityHelpers.SceneController;
using DG.Tweening;
using UnityEngine;
using System;

public class Vertical_ScreenAnimation : BaseScreenAnimation
{
    [SerializeField] private CanvasGroup canvasGroup;

    public override void CloseWindow(Action OnComplete)
    {
        canvasGroup.DOFade(0f, 0.3f).OnComplete(() =>
        {
            OnComplete.Invoke();
        });
    }

    public override void OpenWindow(Action OnComplete)
    {
        canvasGroup.DOFade(1f, 0.3f).OnComplete(() =>
        {
            OnComplete.Invoke();
        });
    }
}