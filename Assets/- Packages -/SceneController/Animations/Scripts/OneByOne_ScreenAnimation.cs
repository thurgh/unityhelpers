﻿using UnityHelpers.SceneController;
using System.Collections.Generic;
using UnityHelpers.RayBlocker;
using System.Collections;
using DG.Tweening;
using UnityEngine;
using System;

[Serializable]
public struct ScreenAnimData
{
    public bool xAxis;
    public RectTransform rectTransform;
    public float openPosition;
    public float closePosition;
}

public class OneByOne_ScreenAnimation : BaseScreenAnimation
{
    [SerializeField] private List<ScreenAnimData> animDatas;

    public override void OpenWindow(Action OnComplete = null)
    {
        StartCoroutine(OpenWindowAsync(OnComplete));
    }

    public IEnumerator OpenWindowAsync(Action OnComplete = null)
    {
        RayBlockerController.Open();
        InstantCloseWindow();

        foreach (var animData in animDatas)
        {
            if (animData.xAxis)
            {
                animData.rectTransform.DOAnchorPosX(animData.openPosition, 0.3f).SetEase(Ease.OutBack);
                yield return new WaitForSeconds(0.1f);
            }
            else
            {
                animData.rectTransform.DOAnchorPosY(animData.openPosition, 0.3f).SetEase(Ease.OutBack);
                yield return new WaitForSeconds(0.1f);
            }
        }

        RayBlockerController.Close();
        OnComplete?.Invoke();
    }

    public override void CloseWindow(Action OnComplete = null)
    {
        StartCoroutine(CloseWindowAsync(true, OnComplete));
    }

    public IEnumerator CloseWindowAsync(bool waitFinishAnim = false, Action OnComplete = null)
    {
        RayBlockerController.Open();

        InstantOpenWindow();

        foreach (var animData in animDatas)
        {
            if (animData.xAxis)
            {
                animData.rectTransform.DOAnchorPosX(animData.closePosition, 0.3f).SetEase(Ease.OutBack);
                yield return new WaitForSeconds(0.1f);
            }
            else
            {
                animData.rectTransform.DOAnchorPosY(animData.closePosition, 0.3f).SetEase(Ease.OutBack);
                yield return new WaitForSeconds(0.1f);
            }
        }

        if (waitFinishAnim)
        {
            yield return new WaitForSeconds(0.2f);
        }

        RayBlockerController.Close();
        OnComplete?.Invoke();
    }

    public void InstantOpenWindow()
    {
        foreach (var animData in animDatas)
        {
            if (animData.xAxis)
            {
                animData.rectTransform.DOAnchorPosX(animData.openPosition, 0f).SetEase(Ease.OutBack);
            }
            else
            {
                animData.rectTransform.DOAnchorPosY(animData.openPosition, 0f).SetEase(Ease.OutBack);
            }
        }
    }

    public void InstantCloseWindow()
    {
        foreach (var animData in animDatas)
        {
            if (animData.xAxis)
            {
                animData.rectTransform.DOAnchorPosX(animData.closePosition, 0f).SetEase(Ease.OutBack);
            }
            else
            {
                animData.rectTransform.DOAnchorPosY(animData.closePosition, 0f).SetEase(Ease.OutBack);
            }
        }
    }
}
