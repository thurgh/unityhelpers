﻿using UnityHelpers.SceneController;
using System;

public struct NoneAnimationData { }

public class NoneAnimData_ScreenAnimation : BaseScreenAnimationWithAnimData<NoneAnimationData>
{
    public override void OpenWindow(NoneAnimationData animData, Action OnComplete = null)
    {
        OnComplete?.Invoke();
    }

    public override void CloseWindow(NoneAnimationData data, Action OnComplete = null)
    {
        OnComplete?.Invoke();
    }
}