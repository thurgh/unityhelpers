﻿using UnityHelpers.SceneController;
using UnityHelpers.RayBlocker;
using DG.Tweening;
using UnityEngine;
using System;

public class Horizontal_ScreenAnimation : BaseScreenAnimation
{
    [SerializeField] private bool initialRightMove = true;
    [SerializeField] private RectTransform screen;

    public override void OpenWindow(Action OnComplete = null)
    {
        RayBlockerController.Open();
        InstantCloseWindow(initialRightMove);

        screen.DOLocalMoveX(0, 0.3f).OnComplete(() =>
        {
            RayBlockerController.Close();
            OnComplete?.Invoke();
        });
    }

    public override void CloseWindow(Action OnComplete = null)
    {
        RayBlockerController.Open();
        InstantOpenWindow();

        float width = initialRightMove ? screen.rect.width : -screen.rect.width;
        screen.DOLocalMoveX(width, 0.3f).OnComplete(() =>
        {
            RayBlockerController.Close();
            OnComplete?.Invoke();
        });
    }

    public void InstantOpenWindow()
    {
        screen.DOLocalMoveX(0f, 0f);
    }

    public void InstantCloseWindow(bool rightMove)
    {
        float width = rightMove ? screen.rect.width : -screen.rect.width;
        screen.DOLocalMoveX(width, 0f);
    }
}