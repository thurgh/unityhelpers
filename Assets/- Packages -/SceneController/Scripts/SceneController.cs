using UnityEngine.SceneManagement;
using UnityEngine;
using System;

namespace UnityHelpers.SceneController
{
    public abstract class SceneController<T> : BaseSceneController<T> where T : struct
    {
        [SerializeField] private BaseScreenAnimation screenAnimation;

        public override void Open(T data, Action OnComplete)
        {
            screenAnimation.OpenWindow(OnComplete);
        }

        public override void Close(T data, string sceneName, Action OnComplete)
        {
            screenAnimation.CloseWindow(() =>
            {
                OnComplete?.Invoke();
                SceneManager.UnloadSceneAsync(sceneName);
            });
        }

        public override void OnStart(T data)
        {
            Debug.Log("- Start -");
        }

        public override void OnOpen(T data)
        {
            Debug.Log("- Open Complete -");
        }

        public override void OnClose(T data)
        {
            Debug.Log("- Close Complete -");
        }
    }
}