using UnityEngine.SceneManagement;
using UnityEngine;
using System;

namespace UnityHelpers.SceneController
{
    public abstract class SceneControllerWithAnimData<T, F> : BaseSceneControllerWithAnimData<T, F> where T : struct where F : struct
    {
        [SerializeField] private BaseScreenAnimationWithAnimData<F> screenAnimation;

        public override void Open(T data, F animData, Action OnComplete)
        {
            screenAnimation.OpenWindow(animData, OnComplete);
        }

        public override void Close(T data, F animData, string sceneName, Action OnComplete)
        {
            screenAnimation.CloseWindow(animData, OnComplete: () =>
            {
                OnComplete?.Invoke();
                SceneManager.UnloadSceneAsync(sceneName);
            });
        }

        public override void OnStart(T data)
        {
            Debug.Log("- Start -");
        }

        public override void OnOpen(T data)
        {
            Debug.Log("- Open Complete -");
        }

        public override void OnClose(T data)
        {
            Debug.Log("- Close Complete -");
        }
    }
}