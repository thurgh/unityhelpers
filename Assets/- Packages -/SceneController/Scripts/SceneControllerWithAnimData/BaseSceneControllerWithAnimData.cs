using UnityEngine.SceneManagement;
using UnityEngine;
using Blazewing;
using System;

namespace UnityHelpers.SceneController
{
    public abstract class BaseSceneControllerWithAnimData<T, F> : MonoBehaviour where T : struct where F : struct
    {
        public static T data;
        public static F animData;
        public static string hideSceneName;

        public abstract void Open(T data, F animData, Action OnComplete);
        public abstract void Close(T data, F animData, string sceneName, Action OnComplete);

        public abstract void OnStart(T data);
        public abstract void OnOpen(T data);
        public abstract void OnClose(T data);

        public static void Show(T data, F animData, string sceneName, LoadSceneMode mode = LoadSceneMode.Single)
        {
            DataController.Add(data);
            DataController.Add(animData);
            SceneManager.LoadSceneAsync(sceneName, mode);
        }

        private void Start()
        {
            data = DataController.Get<T>();
            DataController.Remove<T>();

            animData = DataController.Get<F>();
            DataController.Remove<F>();

            OnStart(data);
            Open(data, animData, () => OnOpen(data));
        }

        public static void Hide(string sceneName)
        {
            hideSceneName = sceneName;
            DataEvent.Notify(data);
        }

        private void OnHide(T data)
        {
            Close(data, animData, hideSceneName, () => OnClose(data));
        }

        protected virtual void OnEnable()
        {
            DataEvent.Register<T>(OnHide);
        }

        protected virtual void OnDisable()
        {
            DataEvent.Unregister<T>(OnHide);
        }
    }
}