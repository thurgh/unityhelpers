﻿using UnityEngine;
using System;

namespace UnityHelpers.SceneController
{
    public abstract class BaseScreenAnimationWithAnimData<T> : MonoBehaviour where T : struct
    {
        public abstract void OpenWindow(T animData, Action OnComplete);
        public abstract void CloseWindow(T animData, Action OnComplete);
    }
}