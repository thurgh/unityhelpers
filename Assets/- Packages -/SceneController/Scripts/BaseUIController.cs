﻿using UnityEngine;
using Blazewing;
using System;

namespace UnityHelpers.SceneController
{
    public abstract class BaseUIController<T> : MonoBehaviour where T : struct
    {
        public static T data;
        public static string hideSceneName;

        public abstract void Open(T data, Action OnComplete);
        public abstract void Close(T data, Action OnComplete);

        public abstract void OnOpen(T data);
        public abstract void OnClose(T data);

        public static void Show(T data)
        {
            DataEvent.Notify(new OnShowStruct(data));
        }

        private void OnShow(OnShowStruct data)
        {
            Open(data.data, () => OnOpen(data.data));
        }

        public static void Hide()
        {
            DataEvent.Notify(new OnHideStruct(data));
        }

        private void OnHide(OnHideStruct data)
        {
            Close(data.data, () => OnClose(data.data));
        }

        protected virtual void OnEnable()
        {
            DataEvent.Register<OnShowStruct>(OnShow);
            DataEvent.Register<OnHideStruct>(OnHide);
        }

        protected virtual void OnDisable()
        {
            DataEvent.Unregister<OnShowStruct>(OnShow);
            DataEvent.Unregister<OnHideStruct>(OnHide);
        }

        public struct OnHideStruct
        {
            public T data;

            public OnHideStruct(T data)
            {
                this.data = data;
            }
        }

        public struct OnShowStruct
        {
            public T data;

            public OnShowStruct(T data)
            {
                this.data = data;
            }
        }
    }
}