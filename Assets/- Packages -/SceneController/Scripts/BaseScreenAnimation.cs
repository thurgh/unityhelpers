using UnityEngine;
using System;

namespace UnityHelpers.SceneController
{
    public abstract class BaseScreenAnimation : MonoBehaviour
    {
        public abstract void OpenWindow(Action OnComplete);
        public abstract void CloseWindow(Action OnComplete);
    }
}
