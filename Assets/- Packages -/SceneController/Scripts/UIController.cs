﻿using UnityEngine;
using System;

namespace UnityHelpers.SceneController
{
    public abstract class UIController<T> : BaseUIController<T> where T : struct
    {
        [SerializeField] private BaseScreenAnimation screenAnimation;

        public override void Open(T data, Action OnComplete)
        {
            screenAnimation.OpenWindow(OnComplete);
        }

        public override void Close(T data, Action OnComplete)
        {
            screenAnimation.CloseWindow(() =>
            {
                OnComplete?.Invoke();
            });
        }

        public override void OnOpen(T data)
        {
            Debug.Log("- Open Complete -");
        }

        public override void OnClose(T data)
        {
            Debug.Log("- Close Complete -");
        }
    }
}