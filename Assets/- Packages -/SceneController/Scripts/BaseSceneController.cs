using UnityEngine.SceneManagement;
using UnityEngine;
using Blazewing;
using System;

namespace UnityHelpers.SceneController
{
    public abstract class BaseSceneController<T> : MonoBehaviour where T : struct
    {
        public static T data;
        public static string hideSceneName;

        public abstract void Open(T data, Action OnComplete);
        public abstract void Close(T data, string sceneName, Action OnComplete);

        public abstract void OnStart(T data);
        public abstract void OnOpen(T data);
        public abstract void OnClose(T data);

        public static AsyncOperation Show(T data, string sceneName, LoadSceneMode mode = LoadSceneMode.Single)
        {
            DataController.Add(data);
            return SceneManager.LoadSceneAsync(sceneName, mode);
        }

        private void Start()
        {
            data = DataController.Get<T>();
            DataController.Remove<T>();

            OnStart(data);
            Open(data, () => OnOpen(data));
        }

        public static void Hide(string sceneName)
        {
            hideSceneName = sceneName;
            DataEvent.Notify(data);
        }

        private void OnHide(T data)
        {
            Close(data, hideSceneName, () => OnClose(data));
        }

        protected virtual void OnEnable()
        {
            DataEvent.Register<T>(OnHide);
        }

        protected virtual void OnDisable()
        {
            DataEvent.Unregister<T>(OnHide);
        }
    }
}