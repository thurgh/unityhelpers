using UnityEditor.SceneManagement;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEditor;
using UnityEngine;
using System.IO;

namespace UnityHelpers.SceneController
{
    public class SceneControllerGenerator : EditorWindow
    {
        private const string TEMPLATE = @"
    using UnityHelpers.SceneController;
    using UnityEngine.UI;
    using UnityEngine;
    using System;

    public struct {0}Data
    {{
        public Action OnOpen;
        public Action OnClose;

        public {0}Data(Action onOpen = null, Action onClose = null)
        {{
            OnOpen = onOpen;
            OnClose = onClose;
        }}
    }}

    public class {0}Controller : SceneController<{0}Data>
    {{
        public const string NAME_SCENE = ""{0}"";

        [SerializeField] private Button closeButton;

        public static void Show({0}Data data = new {0}Data())
        {{
            Show(data, NAME_SCENE);
        }}

        public static void Hide(Action OnClose = null)
        {{
            data.OnClose += OnClose;
            Hide(NAME_SCENE);
        }}

        public override void OnStart({0}Data data)
        {{
            closeButton.onClick.AddListener(OnCloseButtonClick);
        }}

        public override void OnOpen({0}Data data)
        {{
            data.OnOpen?.Invoke();
        }}

        public override void OnClose({0}Data data)
        {{
            data.OnClose?.Invoke();
        }}

        private void OnCloseButtonClick()
        {{
            Hide();
        }}
    }}";

        private string scriptName = "ScriptName";

        [MenuItem("Window/Scene Controller Generator")]
        public static void ShowWindow()
        {
            GetWindow<SceneControllerGenerator>("Scene Controller Generator");
        }

        private void OnGUI()
        {
            scriptName = EditorGUILayout.TextField("Script Name", scriptName);

            if (GUILayout.Button("Generate"))
            {
                string mainFolderPath = CreateFolder(scriptName, "Assets");
                GenerateScript(mainFolderPath);
                GenerateScene(mainFolderPath);
            }
        }

        private void GenerateScript(string mainFolderPath)
        {
            string scriptsFolderPath = CreateFolder("Scripts", mainFolderPath);

            string scriptPath = $"{scriptsFolderPath}/" + scriptName + "Controller.cs";
            string scriptContent = string.Format(TEMPLATE, scriptName);

            File.WriteAllText(scriptPath, scriptContent);
            AssetDatabase.Refresh();
        }

        private void GenerateScene(string mainFolderPath)
        {
            string scenesFolderPath = CreateFolder("Scenes", mainFolderPath);

            Scene newScene = EditorSceneManager.NewScene(NewSceneSetup.DefaultGameObjects, NewSceneMode.Single);

            EditorSceneManager.SetActiveScene(newScene);

            new GameObject("- Controllers -");

            GameObject canvasGO = new GameObject("Canvas");

            Canvas canvas = canvasGO.AddComponent<Canvas>();

            canvas.renderMode = RenderMode.ScreenSpaceOverlay;

            CanvasScaler canvasScaler = canvasGO.AddComponent<CanvasScaler>();

            canvasScaler.uiScaleMode = CanvasScaler.ScaleMode.ScaleWithScreenSize;

            canvasScaler.referenceResolution = new Vector2(1920, 1080);

            canvasScaler.screenMatchMode = CanvasScaler.ScreenMatchMode.Expand;

            canvasGO.AddComponent<GraphicRaycaster>();

            string scenePath = $"{scenesFolderPath}/{scriptName}.unity";

            EditorSceneManager.SaveScene(newScene, scenePath);

            AssetDatabase.Refresh();
        }

        private string CreateFolder(string folderName, string folderPath)
        {
            string fullPath = $"{folderPath}/{folderName}";

            if (!Directory.Exists(fullPath))
            {
                Directory.CreateDirectory(fullPath);
            }

            return fullPath;
        }
    }
}