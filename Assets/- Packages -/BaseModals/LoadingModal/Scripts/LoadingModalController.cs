﻿using UnityHelpers.SceneController;
using UnityEngine.SceneManagement;
using System;

public struct LoadingModalData
{
    public Action OnOpen;
    public Action OnClose;

    public LoadingModalData(Action onOpen = null, Action onClose = null)
    {
        OnOpen = onOpen;
        OnClose = onClose;
    }
}

public class LoadingModalController : SceneController<LoadingModalData>
{
    public const string NAME_SCENE = "Loading_Modal";

    public static async void Show(LoadingModalData data)
    {
        Show(data, NAME_SCENE, LoadSceneMode.Additive);
    }

    public static async void Hide(Action onClose = null)
    {
        data.OnClose += onClose;
        Hide(NAME_SCENE);
    }

    public override void OnStart(LoadingModalData data)
    {
        
    }

    public override void OnOpen(LoadingModalData data)
    {
        data.OnOpen?.Invoke();
    }

    public override void OnClose(LoadingModalData data)
    {
        data.OnClose?.Invoke();
    }
}