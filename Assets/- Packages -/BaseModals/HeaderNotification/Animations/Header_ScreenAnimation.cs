﻿using UnityHelpers.SceneController;
using System.Collections;
using DG.Tweening;
using UnityEngine;
using System;

public class Header_ScreenAnimation : BaseScreenAnimation
{
    [SerializeField] public RectTransform modal;

    public override void OpenWindow(Action OnComplete = null)
    {
        InstantCloseWindow();

        modal.DOAnchorPosY(-180f, 0.5f).SetEase(Ease.OutBack).OnComplete(() =>
        {
            StartCoroutine(Timer(3, () => HeaderNotificationModalController.Hide()));
        });
    }

    public override void CloseWindow(Action OnComplete = null)
    {
        InstantOpenWindow();

        modal.DOAnchorPosY(130f, 0.5f).SetEase(Ease.InBack).OnComplete(() =>
        {
            OnComplete?.Invoke();
        });
    }

    public void InstantOpenWindow()
    {
        modal.DOAnchorPosY(-180f, 0f);
    }

    public void InstantCloseWindow()
    {
        modal.DOAnchorPosY(130f, 0f);
    }

    private IEnumerator Timer(float seconds, Action OnComplete)
    {
        yield return new WaitForSeconds(seconds);

        OnComplete?.Invoke();
    }
}