using UnityHelpers.SceneController;
using UnityEngine.SceneManagement;
using UnityEngine;
using System;
using TMPro;

public struct HeaderNotificationModalData
{
    public string Message;
    public Action OnOpen;
    public Action OnClose;

    public HeaderNotificationModalData(string message, Action onOpen = null, Action onClose = null)
    {
        Message = message;
        OnClose = onClose;
        OnOpen = onOpen;
    }
}

public class HeaderNotificationModalController : SceneController<HeaderNotificationModalData>
{
    public const string NAME_SCENE = "HeaderNotification_Modal";

    [Header("- Confirm Modal -")]
    [SerializeField] private TextMeshProUGUI message;

    public static void Show(HeaderNotificationModalData data)
    {
        Show(data, NAME_SCENE, LoadSceneMode.Additive);
    }

    public static void Hide(Action OnClose = null)
    {
        data.OnClose += OnClose;
        Hide(NAME_SCENE);
    }

    public override void OnStart(HeaderNotificationModalData data)
    {
        message.text = data.Message;
    }

    public override void OnOpen(HeaderNotificationModalData data)
    {
        data.OnOpen?.Invoke();
    }

    public override void OnClose(HeaderNotificationModalData data)
    {
        data.OnClose?.Invoke();
    }
}