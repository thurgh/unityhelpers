using UnityHelpers.SceneController;
using UnityEngine.SceneManagement;
using UnityEngine;
using System;
using TMPro;

public struct NotificationModalData
{
    public string title;
    public string message;
    public Action OnOpen;
    public Action OnConfirm;
    public Action OnCloseConfirm;

    public NotificationModalData(string title, string message, Action onConfirm = null, Action onOpen = null, Action onCloseConfirm = null)
    {
        this.title = title;
        this.message = message;
        this.OnConfirm = onConfirm;
        this.OnCloseConfirm = onCloseConfirm;
        this.OnOpen = onOpen;
    }
}

public class NotificationModalController : SceneController<NotificationModalData>
{
    public const string NAME_SCENE = "Notification_Modal";

    [Header("- Confirm Modal -")]
    [SerializeField] private UI_Button confirmButton;
    [SerializeField] private TextMeshProUGUI message;
    [SerializeField] private TextMeshProUGUI title;

    //[Header("- Audios -")]
    //[SerializeField] private SoundScriptableBase _confirmButtonSound;
    //[SerializeField] private SoundScriptableBase _closeButtonSound;

    public static void Show(NotificationModalData data)
    {
        Show(data, NAME_SCENE, LoadSceneMode.Additive);
    }

    public static void Hide(Action OnClose = null)
    {
        data.OnCloseConfirm += OnClose;
        Hide(NAME_SCENE);
    }

    public override void OnStart(NotificationModalData data)
    {
        title.text = data.title;
        message.text = data.message;
        confirmButton.OnClickAddListener(() => OnConfirmButtonClick(data));
    }

    public override void OnOpen(NotificationModalData data)
    {
        data.OnOpen?.Invoke();
    }

    public override void OnClose(NotificationModalData data)
    {
        data.OnCloseConfirm?.Invoke();
    }

    private void OnConfirmButtonClick(NotificationModalData data)
    {
        //AudioManager.PlayEffect(_confirmButtonSound);
        data.OnConfirm?.Invoke();
        Hide(data.OnCloseConfirm);
    }
}