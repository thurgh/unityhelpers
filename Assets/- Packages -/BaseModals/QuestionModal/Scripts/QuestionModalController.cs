using UnityHelpers.SceneController;
using UnityEngine.SceneManagement;
using UnityHelpers.Extensions;
using UnityEngine;
using System;
using TMPro;

public struct QuestionModalData
{
    public string title;
    public string message;
    public string yesText;
    public string noText;

    public Action OnYesButtonClick;
    public Action OnNoButtonClick;

    public Action OnCloseYesButtonClick;
    public Action OnCloseNoButtonClick;

    public Action OnOpen;
    public Action OnClose;

    public QuestionModalData(string title, string message, string yesText, string noText, Action onYesButtonClick = null, Action onNoButtonClick = null, Action onCloseYesButtonClick = null, Action onCloseNoButtonClick = null, Action onOpen = null, Action onClose = null)
    {
        this.title = title;
        this.message = message;
        this.yesText = yesText;
        this.noText = noText;

        OnYesButtonClick = onYesButtonClick;
        OnNoButtonClick = onNoButtonClick;

        OnCloseYesButtonClick = onCloseYesButtonClick;
        OnCloseNoButtonClick = onCloseNoButtonClick;

        OnOpen = onOpen;
        OnClose = onClose;
    }
}

public class QuestionModalController : SceneController<QuestionModalData>
{
    public const string NAME_SCENE = "Question_Modal";

    [Header("- Selection Modal -")]
    [SerializeField] private TextMeshProUGUI title;
    [SerializeField] private TextMeshProUGUI message;

    [SerializeField] private UI_Button yesButton;
    [SerializeField] private UI_Button noButton;

    [SerializeField] private RectTransform buttonsContent;

    //[Header("- Audios -")]
    //[SerializeField] private SoundScriptableBase _yesButtonSound;
    //[SerializeField] private SoundScriptableBase _noButtonSound;
    //[SerializeField] private SoundScriptableBase _closeButtonSound;

    public static void Show(QuestionModalData data)
    {
        Show(data, NAME_SCENE, LoadSceneMode.Additive);
    }

    public static void Hide(Action OnClose = null)
    {
        data.OnClose += OnClose;
        Hide(NAME_SCENE);
    }

    public override void OnStart(QuestionModalData data)
    {
        title.text = data.title;
        message.text = data.message;

        yesButton.SetStateText(data.yesText);
        noButton.SetStateText(data.noText);

        yesButton.OnClickAddListener(() => OnYesButtonClick(data));
        noButton.OnClickAddListener(() => OnNoButtonClick(data));

        Extensions.ForceUpdate(buttonsContent);
    }

    public override void OnOpen(QuestionModalData data)
    {
        data.OnOpen?.Invoke();
    }

    public override void OnClose(QuestionModalData data)
    {
        data.OnClose?.Invoke();
    }

    private void OnYesButtonClick(QuestionModalData data)
    {
        //AudioManager.PlayEffect(_yesButtonSound);
        data.OnYesButtonClick?.Invoke();
        Hide(data.OnCloseYesButtonClick);
    }

    private void OnNoButtonClick(QuestionModalData data)
    {
        //AudioManager.PlayEffect(_noButtonSound);
        data.OnNoButtonClick?.Invoke();
        Hide(data.OnCloseNoButtonClick);
    }

    private void OnCloseButtonClick()
    {
        //AudioManager.PlayEffect(_closeButtonSound);
        Hide();
    }
}