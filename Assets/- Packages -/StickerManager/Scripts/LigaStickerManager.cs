using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using UnityEngine;
using UnityEngine.UI;

public static class LigaStickerManager
{

#if UNITY_IOS

    [DllImport("__Internal")]
    private static extern bool LigaShareStickerPack(string json);

#endif

    /// Compartilha um sticker pack no WhatsApp. O JSON deve seguir o formato de exemplo
    public static bool ShareStickerPack(string json)
    {
#if UNITY_ANDROID

        AndroidJavaClass unityPlayer = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
        AndroidJavaObject unityActivity = unityPlayer.GetStatic<AndroidJavaObject>("currentActivity");

        AndroidJavaObject ligaStickers = new AndroidJavaObject("liga.plugin.unity.whatsappstickers.LigaStickerManager");

        return ligaStickers.Call<bool>("share", unityActivity, json);

#elif UNITY_IOS

        LigaShareStickerPack(json);
        return false;

#else

        Debug.Log("Sticker packs não são suportados nessa plataforma");
        return false;

#endif
    }

    /// Atualiza o wallpaper do usuário. Apenas Android é suportado.
    private static bool SetWallpaper(string base64)
    {
#if UNITY_ANDROID

        AndroidJavaClass unityPlayer = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
        AndroidJavaObject unityActivity = unityPlayer.GetStatic<AndroidJavaObject>("currentActivity");

        AndroidJavaObject ligaStickers = new AndroidJavaObject("liga.plugin.unity.whatsappstickers.LigaWallpaperManager");

        return ligaStickers.Call<bool>("setWallpaper", unityActivity, base64);

#elif UNITY_IOS

        Debug.Log("Alterar wallpaper não é suportado no iOS");
        return false;

#else

        Debug.Log("Wallpaper não é suportado nessa plataforma");
        return false;

#endif
    }

}
