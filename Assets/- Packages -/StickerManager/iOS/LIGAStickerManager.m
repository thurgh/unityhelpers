//
//  LIGAStickerManager.m
//  WhatsAppStickers
//
//  Created by Guilherme Chaguri on 04/03/22.
//

#import <UIKit/UIKit.h>

bool LigaShareStickerPack(const char *json) {
    @autoreleasepool {
        // Converte para o formato correto para copiar para o Pasteboard
        NSString* jsonString = [NSString stringWithUTF8String: json];
        NSData* jsonData = [jsonString dataUsingEncoding:NSUTF8StringEncoding];

        // Copia as informações do JSON para o Pasteboard, que é a forma na qual o Whatsapp lê as informações do Sticker Pack no iOS
        [[UIPasteboard generalPasteboard] setData:jsonData forPasteboardType:@"net.whatsapp.third-party.sticker-pack"];

        UIApplication *application = [UIApplication sharedApplication];

        // O URL Scheme usado para enviar as informações do Sticker Pack
        NSURL *URL = [NSURL URLWithString:@"whatsapp://stickerPack"];
        
        // Abre o URL Scheme, e por consequencia, abre o WhatsApp
        [application openURL:URL options:@{} completionHandler:^(BOOL success) {
            if (success) {
                printf("Sucesso ao enviar sticker pack ao WhatsApp");
            } else {
                printf("Falha ao enviar o sticker pack ao WhatsApp");
            }
        }];
    }
    
    return true;
}
