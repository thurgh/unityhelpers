using UnityEngine.XR.ARFoundation;
using UnityEngine.XR.ARSubsystems;
using System.Collections.Generic;
using UnityEngine;
using System;

public class ArTrackingCodeController : MonoBehaviour
{
    public Action<string> OnScanCode;

    #region Private methods

    #region Lifecycle

    private void Awake() => AssignGameObjects();

    private void OnEnable() => trackedImageManager.trackedImagesChanged += ImageChanged;

    private void OnDisable() => trackedImageManager.trackedImagesChanged -= ImageChanged;

    #endregion

    private void ImageChanged(ARTrackedImagesChangedEventArgs eventArgs)
    {
        foreach (var trackedImage in eventArgs.updated)
        {
            var obj = spawnedCodes[trackedImage.referenceImage.name];

            switch (trackedImage.trackingState)
            {
                case TrackingState.None:
                case TrackingState.Limited:
                    break;

                case TrackingState.Tracking:
                    OnScanCode?.Invoke(obj);
                    break;

                default:
                    throw new ArgumentOutOfRangeException(nameof(eventArgs));
            }
        }
    }

    private void AssignGameObjects()
    {
        List<string> gameObjects = new List<string>();

        for (int i = 0; i < trackedImageManager.referenceLibrary.count; i++)
        {
            string triggerName = trackedImageManager.referenceLibrary[i].name;

            spawnedCodes.Add(triggerName, triggerName);
        }
    }

    private static IEnumerable<XRReferenceImage> GetImages(IReferenceImageLibrary library)
    {
        for (var i = 0; i < library.count; i++)
        {
            yield return library[i];
        }
    }

    #endregion

    private readonly Dictionary<string, string> spawnedCodes = new Dictionary<string, string>();

    [SerializeField] private ARTrackedImageManager trackedImageManager;
}
