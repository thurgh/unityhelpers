﻿using UnityEngine.SceneManagement;
using Blazewing;

public abstract class BaseDataController<T> : BaseController where T : struct
{
    public abstract void Initialize(T data);

    public static void Show(T data, string sceneName, LoadSceneMode mode, bool ignoreDuplicatedScenes = false)
    {
        DataController.Add(data);
        
        bool alreadyLoaded = Show(sceneName, mode, ignoreDuplicatedScenes);

        if (alreadyLoaded)
        {
            FindObjectOfType<BaseDataController<T>>().Initialize(data);
        }
    }

    private void Start()
    {
        var data = DataController.Get<T>();
        Initialize(data);
    }
}