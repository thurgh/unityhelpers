using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using UnityEngine;

public static class InvalidNickNames
{
    private static TextAsset _badWordsTextAsset = Resources.Load<TextAsset>("badWordsFilter");

    public static List<string> GetBadWordsList()
    {
        var badWords = new List<string>();

        var words = _badWordsTextAsset.text.Split('\n');

        foreach (var word in words)
        {
            badWords.Add(word.Replace("\r", ""));
        }

        return badWords;
    }

    public static bool CheckBadWord(string word)
    {
        var badNames = GetBadWordsList();

        var nameToCheck = Regex.Replace(word.ToLower(), " ", "");

        nameToCheck = Regex.Replace(nameToCheck, "[4@àá]", "a");
        nameToCheck = Regex.Replace(nameToCheck, "[3éè]", "e");
        nameToCheck = Regex.Replace(nameToCheck, "[1|íì]", "i");
        nameToCheck = Regex.Replace(nameToCheck, "[0óò]", "o");
        nameToCheck = Regex.Replace(nameToCheck, "[8úù]", "u");
        nameToCheck = Regex.Replace(nameToCheck, "7", "t");
        nameToCheck = Regex.Replace(nameToCheck, "5", "s");
        nameToCheck = Regex.Replace(nameToCheck, "k", "c");

        nameToCheck = Regex.Replace(nameToCheck, "[a]{25}", "a");
        nameToCheck = Regex.Replace(nameToCheck, "[e]{25}", "e");
        nameToCheck = Regex.Replace(nameToCheck, "[i]{25}", "i");
        nameToCheck = Regex.Replace(nameToCheck, "[o]{25}", "o");
        nameToCheck = Regex.Replace(nameToCheck, "[u]{25}", "u");
        nameToCheck = Regex.Replace(nameToCheck, "[t]{25}", "t");
        nameToCheck = Regex.Replace(nameToCheck, "[s]{25}", "s");
        nameToCheck = Regex.Replace(nameToCheck, "[c]{25}", "c");

        return badNames.Any(badName => badName.Equals(nameToCheck));
        //return badNames.Any(badName => nameToCheck.Contains(badName));
    }

    public static bool CheckCaracteresSpecial(string texto)
    {
        const string ComAcentos = "_!#$%¨&*()-?:{}][ÄÅÁÂÀÃäáâàãÉÊËÈéêëèÍÎÏÌíîïìÖÓÔÒÕöóôòõÜÚÛüúûùÇç ";

        bool contains = false;
        
        foreach (var chaar in ComAcentos.Where(chaar => texto.Contains(chaar)))
        {
            contains = true;
            break;
        }

        return contains;
    }
}