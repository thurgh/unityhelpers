using System.Text.RegularExpressions;
using UnityEngine;
using System;

public class RegisterVerifier : MonoBehaviour
{
    #region Password

    //https://medium.com/@ikhsanudinhakim/most-used-regex-pattern-for-password-validation-314645912cec
    //Minimum 8 characters at least 1 Uppercase Alphabet, 1 Lowercase Alphabet and 1 Number:
    private static string[] SplitPattern = new string[] { ",", ".", "@", "-" };

    public static string CheckRegisterPassword(string email, string password, string confirmPassword = null)
    {
        if (password.Length < 8)
        {
            return "A senha deve ter no mínimo 8 dígitos.";
        }

        if (password.Length > 20)
        {
            return "A senha deve ter no máximo 20 dígitos.";
        }

        if (!string.IsNullOrEmpty(confirmPassword) && !password.Equals(confirmPassword))
        {
            return "As senhas não coincidem";
        }

        var splitResult = email.Split(SplitPattern, StringSplitOptions.RemoveEmptyEntries);

        for (var i = 0; i < splitResult.Length; i++)
        {
            if (!password.Contains(splitResult[i], StringComparison.InvariantCulture))
            {
                continue;
            }

            return "A senha não deve conter nenhuma palavra presente no e-mail.";
        }

        var hasNumber = new Regex(@"[0-9]+");
        var hasLowerChar = new Regex(@"[a-z]+");
        var hasUpperChar = new Regex(@"[A-Z]+");
        var hasMinimum8Chars = new Regex(@".{8,}");

        bool isValidated = hasNumber.IsMatch(password) && hasLowerChar.IsMatch(password) && hasUpperChar.IsMatch(password) && hasMinimum8Chars.IsMatch(password);

        if (!isValidated)
        {
            return "A senha deve conter pelo menos 1 Letra Maiúscula, 1 Letra Minúscula, e 1 Número";
        }


        return string.Empty;
    }

    public static string CheckPassword(string email, string password)
    {
        if (password.Length < 8)
        {
            return "A senha deve ter no mínimo 8 dígitos.";
        }

        if (password.Length > 20)
        {
            return "A senha deve ter no máximo 20 dígitos.";
        }

        var splitResult = email.Split(SplitPattern, StringSplitOptions.RemoveEmptyEntries);

        for (var i = 0; i < splitResult.Length; i++)
        {
            if (!password.Contains(splitResult[i], StringComparison.InvariantCulture))
            {
                continue;
            }

            return "A senha não deve conter nenhuma palavra presente no e-mail.";
        }

        var hasNumber = new Regex(@"[0-9]+");
        var hasLowerChar = new Regex(@"[a-z]+");
        var hasUpperChar = new Regex(@"[A-Z]+");
        var hasMinimum8Chars = new Regex(@".{8,}");

        bool isValidated = hasNumber.IsMatch(password) && hasLowerChar.IsMatch(password) && hasUpperChar.IsMatch(password) && hasMinimum8Chars.IsMatch(password);

        if (!isValidated)
        {
            return "A senha deve conter pelo menos 1 Letra Maiúscula, 1 Letra Minúscula, e 1 Número";
        }

        return string.Empty;
    }

    #endregion

    #region Email

    private const string MatchEmailPattern = @"^(([\w-]+\.)+[\w-]+|([a-zA-Z]{1}|[\w-]{2,}))@"
    + @"((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\.([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\."
    + @"([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\.([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])){1}|"
    + @"([a-zA-Z]+[\w-]+\.)+[a-zA-Z]{2,4})$";

    public static string CheckEmail(string email)
    {
        if (!Regex.IsMatch(email, MatchEmailPattern))
        {
            return "Email invalido!";
        }

        return string.Empty;
    }

    #endregion

    #region Username

    public static string CheckUsername(string username)
    {
        if (username.Length < 3)
        {
            return "O apelido deve ter no minimo 3 letras";
        }

        if (InvalidNickNames.CheckBadWord(username))
        {
            return "Apelido impróprio";
        }

        if (InvalidNickNames.CheckCaracteresSpecial(username))
        {
            return "Caracteres especiais/acentos inválidos";
        }

        return string.Empty;
    }

    public static string CheckChangeUsername(string currentUsername, string newUsername)
    {
        if (currentUsername == newUsername)
        {
            return "Este já é seu apelido";
        }

        if (newUsername.Length < 3)
        {
            return "O apelido deve ter no minimo 3 letras";
        }

        if (InvalidNickNames.CheckBadWord(newUsername))
        {
            return "Apelido impróprio";
        }

        if (InvalidNickNames.CheckCaracteresSpecial(newUsername))
        {
            return "Caracteres especiais/acentos inválidos";
        }

        return string.Empty;
    }

    #endregion

    #region Login

    public static string CheckLogin(string email, string password)
    {
        string emailError = CheckEmail(email);
        if (emailError.Length > 0)
        {
            return emailError;
        }

        string passwordError = CheckRegisterPassword(email, password);
        if (passwordError.Length > 0)
        {
            return "Email e/ou senha incorretos.";
        }

        return string.Empty;
    }

    #endregion

    #region Register

    public static string CheckRegister(string nickname, string email, string password, string confirmPassword)
    {
        string usernameError = CheckUsername(nickname);
        if (usernameError.Length > 0)
        {
            return usernameError;
        }

        string emailError = CheckEmail(email);
        if (emailError.Length > 0)
        {
            return emailError;
        }

        string passwordError = CheckRegisterPassword(email, password, confirmPassword);
        if (passwordError.Length > 0)
        {
            return passwordError;
        }

        return string.Empty;
    }

    public static string CheckRegister(string email, string password, string confirmPassword)
    {
        string emailError = CheckEmail(email);
        if (emailError.Length > 0)
        {
            return emailError;
        }

        string passwordError = CheckRegisterPassword(email, password, confirmPassword);
        if (passwordError.Length > 0)
        {
            return passwordError;
        }

        return string.Empty;
    }

    #endregion
}