using UnityEngine;
using TMPro;

namespace UnityHelpers.VersionController
{
    public class VersionController : MonoBehaviour
    {
        [SerializeField] private TextMeshProUGUI versionText;

        private void Start()
        {
            versionText.text = $"v{Application.version}";
        }
    }
}