using UnityEngine.Events;
using UnityEngine.UI;
using UnityEngine;
using DG.Tweening;

[RequireComponent(typeof(Button))]
public class UI_Button : MonoBehaviour
{
    [SerializeField] private Button button;
    [SerializeField] private CanvasGroup canvasGroup;
    [SerializeField] private UI_ButtonAnimation anim;
    [SerializeField] private UI_ButtonState state;

    [SerializeField] private bool interactable = true;
    public bool Interactable
    {
        get { return interactable; }
        set
        {
            interactable = value;
            button.interactable = value;
            state?.SetState(value);
        }
    }

    public void OnValidate()
    {
        TryGetComponent(out state);
        TryGetComponent(out anim);
        TryGetComponent(out canvasGroup);

        button = GetComponent<Button>();
        button.transition = Selectable.Transition.None;
        Interactable = interactable;
    }

    private void Awake()
    {
        Interactable = interactable;
    }

    public void OnClickAddListener(UnityAction onClick)
    {
        button.onClick.AddListener(() =>
        {
            anim?.ClickAnimation();
            onClick?.Invoke();
        });
    }

    public void SetStateIcon(Sprite icon)
    {
        state?.SetIcon(icon);
    }

    public void SetStateText(string text)
    {
        state?.SetText(text);
    }

    public void SetButtonImage(Sprite sprite)
    {
        button.image.sprite = sprite;
    }

    public void SetButtonImageColor(Color color)
    {
        button.image.color = color;
    }

    public void SetButtonImageDOColor(Color color, float time)
    {
        button.image.DOColor(color, time);
    }

    public CanvasGroup GetCanvasGroup()
    {
        return canvasGroup;
    }
}