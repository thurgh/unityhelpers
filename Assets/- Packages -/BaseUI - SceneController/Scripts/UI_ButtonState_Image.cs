﻿using UnityEngine.UI;
using UnityEngine;
using TMPro;

public class UI_ButtonState_Image : UI_ButtonState
{
    [SerializeField] private Image buttonImage;
    [SerializeField] private Image buttonIcon;
    [SerializeField] private TextMeshProUGUI buttonText;

    [SerializeField] private Sprite enabledButtonSprite;
    [SerializeField] private Sprite disabledButtonSprite;

    [SerializeField] private Color enabledTextColor;
    [SerializeField] private Color disabledTextColor;

    public override void SetState(bool state)
    {
        if (buttonImage) buttonImage.sprite = state ? enabledButtonSprite : disabledButtonSprite;
        if (buttonIcon) buttonIcon.color = state ? enabledTextColor : disabledTextColor;
        if (buttonText) buttonText.color = state ? enabledTextColor : disabledTextColor;
    }

    public override void SetIcon(Sprite icon)
    {
        if (buttonIcon) buttonIcon.sprite = icon;
    }

    public override void SetText(string text)
    {
        if (buttonText) buttonText.text = text;
    }
}