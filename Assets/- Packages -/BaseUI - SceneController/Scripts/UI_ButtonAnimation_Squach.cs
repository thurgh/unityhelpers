﻿using DG.Tweening;
using UnityEngine;

public class UI_ButtonAnimation_Squach : UI_ButtonAnimation
{
    private RectTransform rectTransform;

    private void Awake()
    {
        rectTransform = GetComponent<RectTransform>();
    }

    public override void ClickAnimation()
    {
        rectTransform.DOScale(1.1f, 0.1f).OnComplete(() => rectTransform.DOScale(1f, 0.05f));
    }

    private void OnValidate()
    {
        gameObject.GetComponent<UI_Button>().OnValidate();
    }
}