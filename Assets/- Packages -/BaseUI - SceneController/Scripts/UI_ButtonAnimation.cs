﻿using UnityEngine;

[RequireComponent(typeof(UI_Button))]
public abstract class UI_ButtonAnimation : MonoBehaviour
{
    public abstract void ClickAnimation();

    private void OnValidate()
    {
        gameObject.GetComponent<UI_Button>().OnValidate();
    }
}
