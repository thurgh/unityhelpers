﻿using UnityEngine;

[RequireComponent(typeof(UI_Button))]
public abstract class UI_ButtonState : MonoBehaviour
{
    public abstract void SetState(bool state);
    public abstract void SetIcon(Sprite icon);
    public abstract void SetText(string text);

    private void OnValidate()
    {
        gameObject.GetComponent<UI_Button>().OnValidate();
    }
}