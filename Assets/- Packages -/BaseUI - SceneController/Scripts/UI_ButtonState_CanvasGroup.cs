﻿using System.Collections.Generic;
using UnityEngine.UI;
using DG.Tweening;
using UnityEngine;

public class UI_ButtonState_CanvasGroup : UI_ButtonState
{
    [SerializeField] private List<Image> buttonImages;
    [SerializeField] private CanvasGroup canvasGroup;
    private float animSpeed = 0.2f;

    private List<Color> originalcolors = new List<Color>();
    private float h, s, v;

    private void Awake()
    {
        foreach (var buttonImage in buttonImages)
        {
            originalcolors.Add(buttonImage.color);
        }
    }

    public override void SetState(bool state)
    {
        if(canvasGroup) canvasGroup.DOFade(state ? 1f : 0.5f, animSpeed);

        if (buttonImages.Count == 0) return;

        if (originalcolors.Count == 0)
        {
            foreach (var buttonImage in buttonImages)
            {
                originalcolors.Add(buttonImage.color);
            }
        }

        if (state)
        {
            for (int i = 0; i < buttonImages.Count; i++)
            {
                buttonImages[i].DOColor(originalcolors[i], animSpeed);
            }
        }
        else
        {
            for (int i = 0; i < buttonImages.Count; i++)
            {
                Color.RGBToHSV(buttonImages[i].color, out h, out s, out v);
                buttonImages[i].DOColor(Color.HSVToRGB(h, s / 2f, v), animSpeed);
            }
        }
    }

    public override void SetIcon(Sprite icon) { }

    public override void SetText(string text) { }
}