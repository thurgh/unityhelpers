using System.Collections.Generic;
using UnityEngine.EventSystems;
using Random = System.Random;
using System.Collections;
using UnityEngine.UI;
using UnityEngine;
using DG.Tweening;
using System;
using UnityEditor;
using System.IO;

namespace UnityHelpers.Extensions
{
    public static class Extensions
    {
        #region RectTransform

        public static void SetLeft(this RectTransform rt, float left)
        {
            rt.offsetMin = new Vector2(left, rt.offsetMin.y);
        }

        public static void SetRight(this RectTransform rt, float right)
        {
            rt.offsetMax = new Vector2(-right, rt.offsetMax.y);
        }

        public static void SetTop(this RectTransform rt, float top)
        {
            rt.offsetMax = new Vector2(rt.offsetMax.x, -top);
        }

        public static void SetBottom(this RectTransform rt, float bottom)
        {
            rt.offsetMin = new Vector2(rt.offsetMin.x, bottom);
        }

        public static void DOSetTop(this RectTransform rt, float top, float duration, Action OnComplete = null)
        {
            float animTop = -rt.offsetMax.y;

            DOTween.To(() => animTop, x => animTop = x, top, duration).OnUpdate(() =>
            {
                SetTop(rt, animTop);
            }).OnComplete(() => OnComplete());
        }

        public static void ForceUpdate(this RectTransform rectTransform)
        {
            LayoutRebuilder.ForceRebuildLayoutImmediate(rectTransform);
        }

        #endregion

        #region UI

        public static bool PointerOverUI(List<RectTransform> uiRaycastsIgnore)
        {
            PointerEventData eventDataCurrentPosition = new PointerEventData(EventSystem.current);
            eventDataCurrentPosition.position = new Vector2(Input.mousePosition.x, Input.mousePosition.y);
            List<RaycastResult> rayResults = new List<RaycastResult>();
            EventSystem.current.RaycastAll(eventDataCurrentPosition, rayResults);

            List<RectTransform> uiResults = new List<RectTransform>();
            foreach (var rayResult in rayResults)
            {
                RectTransform rect;
                if (rayResult.gameObject.TryGetComponent(out rect) && !uiRaycastsIgnore.Contains(rect))
                {
                    uiResults.Add(rect);
                }
            }

            return uiResults.Count > 0;
        }

        #endregion

        #region Console

        public static void LogColor(string message, Color colorName)
        {
            LogColor(message, colorName.ToString());
        }

        public static void LogColor(string message, string color = "white")
        {
            Debug.Log($"<color={color}>{message}</color>");
        }

        #endregion

        #region Angles

        public static float GetAbsAngle(float rot)
        {
            while (rot < 0f) rot += 360f;
            while (rot >= 360f) rot -= 360f;
            return rot;
        }

        public static float GetMiddleAngle(float rot)
        {
            while (rot > 180f) rot -= 360f;
            while (rot < -180f) rot += 360f;
            return rot;
        }

        #endregion

        #region Textures

        // Usar texture.EncodeToPNG(); para coletar os Bytes. Ative 'Read/Write' no arquivo da Sprite.
        public static Sprite GetSpriteFromBytes(byte[] bytes)
        {
            Texture2D iconText = new Texture2D(1, 1);
            iconText.LoadImage(bytes);
            iconText.Apply();
            return Sprite.Create(iconText, new Rect(0, 0, iconText.width, iconText.height), new Vector2(0.5f, 0.5f));
        }

        public static Texture2D TextureToTexture2D(Texture texture)
        {
            Texture2D texture2D = new Texture2D(texture.width, texture.height, TextureFormat.RGBA32, false);
            RenderTexture currentRT = RenderTexture.active;
            RenderTexture renderTexture = RenderTexture.GetTemporary(texture.width, texture.height, 32);
            Graphics.Blit(texture, renderTexture);

            RenderTexture.active = renderTexture;
            texture2D.ReadPixels(new Rect(0, 0, renderTexture.width, renderTexture.height), 0, 0);
            texture2D.Apply();

            RenderTexture.active = currentRT;
            RenderTexture.ReleaseTemporary(renderTexture);
            return texture2D;
        }

        // As vezes da uns bugs
        public static Sprite Texture2DToSprite(Texture2D texture2D)
        {
            return texture2D ? Sprite.Create(texture2D, new Rect(0, 0, texture2D.width, texture2D.height), Vector2.zero, 1f) : null;
        }

        public static Texture2D RenderTextureToTexture2D(RenderTexture renderTexture, int width, int height)
        {
            Texture2D tex = new Texture2D(width, height, TextureFormat.RGB24, false);
            // ReadPixels looks at the active RenderTexture.
            RenderTexture.active = renderTexture;
            tex.ReadPixels(new Rect(0, 0, renderTexture.width, renderTexture.height), 0, 0);
            tex.Apply();
            return tex;
        }

        #endregion

        #region Base64

        public static string Encode64(RenderTexture renderTexture)
        {
            Texture2D texture2D = RenderTextureToTexture2D(renderTexture, 128, 128);
            return Encode64(texture2D);
        }

        public static string Encode64(Sprite sprite)
        {
            Texture2D texture2D = sprite.texture;
            return Encode64(texture2D);
        }

        public static string Encode64(Texture2D texture2D)
        {
            byte[] bytes = texture2D.EncodeToPNG();
            return Convert.ToBase64String(bytes);
        }

        public static Sprite Decode64(string base64)
        {
            byte[] bytesBase64 = Convert.FromBase64String(base64);
            Texture2D texture = new Texture2D(1, 1);
            texture.LoadImage(bytesBase64);
            return Texture2DToSprite(texture);
        }

        #endregion

        #region Time

        public static IEnumerator WaitingAsync(float timeToWait, Action OnWait)
        {
            yield return new WaitForSeconds(timeToWait);

            OnWait.Invoke();
        }

        public static string GetTimeBySeconds(float timer, int infos)
        {
            string returnTimer = string.Empty;

            int seconds = (int)(timer % 60f);
            int minutes = (int)(timer / 60f);
            int hours = (int)(minutes / 60f);

            if (infos >= 3) returnTimer += $"{FormatTime(hours)}:";
            if (infos >= 2) returnTimer += $"{FormatTime(minutes)}:";
            if (infos >= 1) returnTimer += FormatTime(seconds);

            return returnTimer;
        }

        public static string FormatTime(float time)
        {
            return time < 10 ? $"0{time}" : $"{time}";
        }

        #endregion

        #region Math

        public static float Remap(this float value, float from1, float to1, float from2, float to2)
        {
            return (value - from1) / (to1 - from1) * (to2 - from2) + from2;
        }

        public static void Shuffle<T>(this IList<T> list)
        {
            Random rng = new Random();

            int n = list.Count;

            while (n > 1)
            {
                n--;
                int k = rng.Next(n + 1);
                T value = list[k];
                list[k] = list[n];
                list[n] = value;
            }
        }

        #endregion

        #region Folders

        public static void CleanUpEmptyFolders(string folderPath)
        {
            // Obter todas as subpastas no caminho fornecido.
            string[] subFolders = Directory.GetDirectories(folderPath);
            Debug.Log($"Folder: {Path.GetFileName(folderPath)} | SubFolders: {subFolders.Length}");

            foreach (string subFolder in subFolders)
            {
                CleanUpEmptyFolders(subFolder); // Chamar recursivamente para processar subpastas.

                // Verificar se a pasta está vazia.
                if (Directory.Exists(subFolder) && Directory.GetFileSystemEntries(subFolder).Length == 0)
                {
                    // Apagar a pasta vazia.
                    DeleteFolder(subFolder);
                }
            }

            // Após apagar as subpastas vazias, verificar se a pasta atual também está vazia.
            if (Directory.Exists(folderPath) && Directory.GetFileSystemEntries(folderPath).Length == 0)
            {
                // Apagar a pasta vazia.
                DeleteFolder(folderPath);
            }

            AssetDatabase.Refresh();
        }

        public static string CreateFolder(string folderName, string folderPath)
        {
            string fullPath = $"{folderPath}/{folderName}";

            if (!Directory.Exists(fullPath))
            {
                Directory.CreateDirectory(fullPath);
            }

            return fullPath;
        }

        public static string CreateFolder(string fullPath)
        {
            if (!Directory.Exists(fullPath))
            {
                Directory.CreateDirectory(fullPath);
            }

            return fullPath;
        }

        public static void DeleteFolder(string folderPath)
        {
            AssetDatabase.Refresh();

            if (Directory.Exists(folderPath))
            {
                Directory.Delete(folderPath, true);
                File.Delete($"{folderPath}.meta");
                AssetDatabase.Refresh();
            }
        }

        #endregion

        // Esse metodo é somente para ficar de facil acesso na hora de utilizar porque não consegui fazer com que o 'value' receba seu valor durante a animação.
        public static void DOFloat(this float value, float endValue, float duration)
        {
            DOTween.To(() => value, x => value = x, endValue, duration).OnUpdate(() => { });
        }
    }
}