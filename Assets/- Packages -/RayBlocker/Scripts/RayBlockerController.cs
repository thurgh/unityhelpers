using UnityEngine.UI;
using UnityEngine;

namespace UnityHelpers.RayBlocker
{
    public class RayBlockerController : Manager
    {
        public static RayBlockerController instance;

        [SerializeField] private GraphicRaycaster graphicRaycaster;
        [SerializeField] private Image image;

        public override void Initialize()
        {
            if (instance == null)
            {
                instance = this;
            }
            else if (instance != this)
            {
                Destroy(gameObject);
            }

            DontDestroyOnLoad(gameObject);
        }

        public static void AwakeShow()
        {
            instance.Show();
        }

        public static void Open()
        {
            instance.Show();
        }

        private void Show()
        {
            instance.graphicRaycaster.enabled = true;
            instance.image.enabled = true;
        }

        public static void Close()
        {
            instance.graphicRaycaster.enabled = false;
            instance.image.enabled = false;
        }
    }
}