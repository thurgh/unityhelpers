﻿using Random = UnityEngine.Random;
using System.Collections.Generic;
using UnityEngine.Audio;
using System.Linq;
using DG.Tweening;
using UnityEngine;

public class AudioManager : Manager
{
    public static AudioManager instance;

    [Header("Audio Manager")]
    [SerializeField] private AudioMixerGroup mainGroup;
    private List<AudioSource> listAudioSource;

    private AudioSource music;

    public const string musicVolumeKey = "VolumeMusic";
    public const string effectVolumeKey = "VolumeEffects";

    #region General

    public override void Initialize()
    {
        if (instance == null)
        {
            instance = this;
        }
        else if (instance != this)
        {
            Destroy(gameObject);
        }

        DontDestroyOnLoad(gameObject);

        listAudioSource = new List<AudioSource>();

        AudioSource newAudioSourceEffect = gameObject.AddComponent<AudioSource>();
        listAudioSource.Add(newAudioSourceEffect);

        music = gameObject.AddComponent<AudioSource>();
    }

    private void Start()
    {
        Load();
    }

    private void Load()
    {
        if (!PlayerPrefs.HasKey(musicVolumeKey)) PlayerPrefs.SetFloat(musicVolumeKey, 1f);
        float loadMusicVolume = PlayerPrefs.GetFloat(musicVolumeKey);
        mainGroup.audioMixer.SetFloat("VolumeMusic", Remap(loadMusicVolume, 0f, 1f, -80f, 0f));

        if (!PlayerPrefs.HasKey(effectVolumeKey)) PlayerPrefs.SetFloat(effectVolumeKey, 1f);
        float loadEffectVolume = PlayerPrefs.GetFloat(effectVolumeKey);
        mainGroup.audioMixer.SetFloat("VolumeEffect", Remap(loadEffectVolume, 0f, 1f, -80f, 0f));
    }

    private void Play(AudioSource audiosource, SoundScriptableBase soundScriptableBase)
    {
        SoundScriptable soundScriptable = soundScriptableBase.GetSoundScriptable();

        audiosource.outputAudioMixerGroup = soundScriptable.group;
        audiosource.loop = soundScriptable.Loop;
        
        audiosource.pitch = soundScriptable.Pitch;
        audiosource.volume = soundScriptable.Volume;
        
        audiosource.clip = soundScriptable.audio;
        audiosource.Play();
    }

    #endregion

    #region Volume

    public static void SetMusicVolume(float value)
    {
        instance.mainGroup.audioMixer.SetFloat("VolumeMusic", instance.Remap(value, 0f, 1f, -80f, 0f));
        PlayerPrefs.SetFloat(musicVolumeKey, value);
    }

    public static void SetEffectVolume(float value)
    {
        instance.mainGroup.audioMixer.SetFloat("VolumeEffect", instance.Remap(value, 0f, 1f, -80f, 0f));
        PlayerPrefs.SetFloat(effectVolumeKey, value);
    }

    #endregion

    #region Base Effects

    public static void PlayEffect(SoundScriptableBase soundScriptableBase)
    {
        SoundScriptable soundScriptable = soundScriptableBase.GetSoundScriptable();

        AudioSource newAudioSource = instance.gameObject.AddComponent<AudioSource>();
        instance.listAudioSource.Add(newAudioSource);

        instance.Play(newAudioSource, soundScriptable);

        DOVirtual.DelayedCall(soundScriptable.audio.length, () =>
        {
            instance.listAudioSource.Remove(newAudioSource);
            Destroy(newAudioSource);
        });
    }

    public static void PlayEffect(AudioSource source, SoundScriptableBase value)
    {
        instance.Play(source, value);
    }

    public static void StopEffect(SoundScriptableBase soundScriptableBase)
    {
        SoundScriptable soundScriptable = soundScriptableBase.GetSoundScriptable();

        foreach (var audioSource in instance.listAudioSource.Where(audioSource => audioSource.clip == soundScriptable.audio))
        {
            audioSource.Stop();
            audioSource.clip = null;
        }
    }

    #endregion

    #region Base Random Effects

    public static void PlayRandomEffect(SoundScriptableBase soundScriptableBase)
    {
        List<SoundScriptable> soundScriptables = soundScriptableBase.GetRandomSoundScriptable();

        if (soundScriptables.Count < 0) return;

        int random = Random.Range(0, soundScriptables.Count);

        PlayEffect(soundScriptables[random]);
    }

    public static void PlayRandomEffect(AudioSource source, SoundScriptableBase soundScriptableBase)
    {
        List<SoundScriptable> soundScriptables = soundScriptableBase.GetRandomSoundScriptable();

        if (soundScriptables.Count < 0) return;

        int random = Random.Range(0, soundScriptables.Count);

        PlayEffect(source, soundScriptables[random]);
    }

    #endregion

    #region Base Music

    public static void PlayMusic(SoundScriptableBase soundScriptableBase, bool useFade = true)
    {
        SoundScriptable soundScriptable = soundScriptableBase.GetSoundScriptable();

        if (instance.music.clip == soundScriptable.audio) return;

        if (instance.music.isPlaying)
        {
            if (useFade)
            {
                instance.music.DOFade(0, 1f).onComplete += () =>
                {
                    instance.Play(instance.music, soundScriptable);
                    instance.music.DOFade(soundScriptable.Volume, 1f);
                };
            }
            else
            {
                instance.Play(instance.music, soundScriptable);
            }
        }
        else
        {
            instance.Play(instance.music, soundScriptable);
            instance.music.DOFade(soundScriptable.Volume, 1f);
        }
    }

    public static void StopMusic()
    {
        instance.music.Stop();
    }

    #endregion

    #region Extensions

    private float Remap(float s, float a1, float a2, float b1, float b2)
    {
        return b1 + (s - a1) * (b2 - b1) / (a2 - a1);
    }

    #endregion
}