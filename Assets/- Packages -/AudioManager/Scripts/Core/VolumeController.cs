﻿using UnityEngine.Audio;
using UnityEngine.UI;
using UnityEngine;

public class VolumeController : MonoBehaviour
{
    [SerializeField] private AudioMixerGroup audioMixerGroup;
    [SerializeField] private Slider musicVolumeSlider;
    [SerializeField] private Slider effectVolumeSlider;

    private void Start()
    {
        float musicVolume;
        float effectVolume;

        audioMixerGroup.audioMixer.GetFloat("VolumeMusic", out musicVolume);
        audioMixerGroup.audioMixer.GetFloat("VolumeEffect", out effectVolume);

        musicVolumeSlider.value = Remap(musicVolume, -80f, 0f, 0f, 1f);
        effectVolumeSlider.value = Remap(effectVolume, -80f, 0f, 0f, 1f);

        musicVolumeSlider.onValueChanged.AddListener(value =>
        {
            AudioManager.SetMusicVolume(value);
        });

        effectVolumeSlider.onValueChanged.AddListener(value =>
        {
            AudioManager.SetEffectVolume(value);
        });
    }

    #region Extensions

    private float Remap(float s, float a1, float a2, float b1, float b2)
    {
        return b1 + (s - a1) * (b2 - b1) / (a2 - a1);
    }

    #endregion
}