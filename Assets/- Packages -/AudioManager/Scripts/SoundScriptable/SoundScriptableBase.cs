﻿using System.Collections.Generic;
using UnityEngine;

public abstract class SoundScriptableBase : ScriptableObject
{
    public abstract SoundScriptable GetSoundScriptable();
    public abstract List<SoundScriptable> GetRandomSoundScriptable();
}
