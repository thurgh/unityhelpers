﻿using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "newSound", menuName = "SoundScriptable/Create Sound Random Variant")]
public class SoundRandomScriptableVariant : SoundScriptableBase
{
    public List<SoundScriptable> soundScriptable;

    public override SoundScriptable GetSoundScriptable()
    {
        return null;
    }

    public override List<SoundScriptable> GetRandomSoundScriptable()
    {
        return soundScriptable;
    }
}