﻿using System.Collections.Generic;
using UnityEngine.Audio;
using UnityEngine;

[CreateAssetMenu(fileName ="newSound", menuName = "SoundScriptable/Create Sound")]
public class SoundScriptable : SoundScriptableBase
{
    public AudioClip audio;
    public AudioMixerGroup group;
    
    [SerializeField] private float volume = 1f;
    [SerializeField] private bool loop;

    public float Volume
    {
        get => volume;
        set => volume = value;
    }

    public bool Loop
    {
        get => loop;
        set => loop = value;
    }

    [Header("Space")]
    [SerializeField] private float pitch = 1f;
    [SerializeField] private bool useRandomPitch = false;
    [SerializeField] private float minPitch = 0.8f;
    [SerializeField] private float maxPitch = 1.2f;

    public float Pitch
    {
        get => useRandomPitch ? Random.Range(minPitch, maxPitch) : pitch;
        set => pitch = value;
    }

    public SoundScriptable()
    {
        this.audio = null;
        Pitch = 1;
        Volume = 1f;
        Loop = false;
    }

    public SoundScriptable(AudioClip _value, float _pitch = 1f, float _volume = 1f, bool _loop = false, AudioMixerGroup _group = null)
    {
        this.audio = _value;
        Pitch = _pitch;
        Volume = _volume;
        Loop = _loop;
        this.group = _group;
    }

    public override SoundScriptable GetSoundScriptable()
    {
        return this;
    }

    public override List<SoundScriptable> GetRandomSoundScriptable()
    {
        return null;
    }
}
