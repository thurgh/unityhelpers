﻿using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "newSound", menuName = "SoundScriptable/Create Sound Variant")]
public class SoundScriptableVariant : SoundScriptableBase
{
    public SoundScriptable soundScriptable;

    public override SoundScriptable GetSoundScriptable()
    {
        return soundScriptable;
    }

    public override List<SoundScriptable> GetRandomSoundScriptable()
    {
        return null;
    }
}