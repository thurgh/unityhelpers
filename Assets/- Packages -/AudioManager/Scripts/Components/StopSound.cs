﻿using UnityEngine;

public class StopSound : MonoBehaviour
{
    [SerializeField] private SoundScriptableBase effectScriptable;

    public void StopEffect()
    {
        AudioManager.StopEffect(effectScriptable);
    }

    public void StopMusic()
    {
        AudioManager.StopMusic();
    }
}