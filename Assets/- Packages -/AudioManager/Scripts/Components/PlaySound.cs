﻿using UnityEngine;

public class PlaySound : MonoBehaviour
{
    [SerializeField] private SoundScriptableBase sound;

    public void PlayEffect()
    {
        AudioManager.PlayEffect(sound);
    }

    public void PlayMusic()
    {
        AudioManager.PlayMusic(sound);
    }
}