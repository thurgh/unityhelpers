﻿using UnityEngine;

public class PlayRandomSound : MonoBehaviour
{
    [SerializeField] private SoundScriptableBase randomSound;

    public void PlayRandomEffect()
    {
        AudioManager.PlayRandomEffect(randomSound);
    }
}