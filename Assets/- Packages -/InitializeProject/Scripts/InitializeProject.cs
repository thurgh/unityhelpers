using System.Collections.Generic;
using UnityEngine;

[DefaultExecutionOrder(-1)]
public class InitializeProject : MonoBehaviour
{
    public static InitializeProject initializeProject;

    public List<Manager> managers = new List<Manager>();

    private void Awake()
    {
        if (initializeProject == null)
        {
            initializeProject = this;
            DontDestroyOnLoad(gameObject);
            Initialize();
        }
        else if(initializeProject != this)
        {
            Destroy(gameObject);
        }
    }

    private void Initialize()
    {
        foreach (var manager in managers)
        {
            Instantiate(manager).Initialize();
        }
    }
}
