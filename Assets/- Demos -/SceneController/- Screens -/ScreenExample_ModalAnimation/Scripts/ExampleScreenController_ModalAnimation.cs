﻿using UnityHelpers.SceneController;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEngine;
using System;

public struct ExampleAnimatedScreenData_ModalAnimation
{
    public Action OnOpen;
    public Action OnClose;

    public ExampleAnimatedScreenData_ModalAnimation(Action onOpen = null, Action onClose = null)
    {
        OnOpen = onOpen;
        OnClose = onClose;
    }
}

public class ExampleScreenController_ModalAnimation : SceneController<ExampleAnimatedScreenData_ModalAnimation>
{
    public const string NAME_SCENE = "ScreenExample_ModalAnimation";

    [SerializeField] private Button backButton;

    public static void Show(ExampleAnimatedScreenData_ModalAnimation data)
    {
        Show(data, NAME_SCENE, LoadSceneMode.Additive);
    }

    public static void Hide()
    {
        Hide(NAME_SCENE);
    }

    public override void OnStart(ExampleAnimatedScreenData_ModalAnimation data)
    {
        backButton.onClick.AddListener(OnBackButton);
    }

    public override void OnOpen(ExampleAnimatedScreenData_ModalAnimation data)
    {
        data.OnOpen?.Invoke();
    }

    public override void OnClose(ExampleAnimatedScreenData_ModalAnimation data)
    {
        data.OnClose?.Invoke();
    }

    private void OnBackButton()
    {
        Hide();
    }
}