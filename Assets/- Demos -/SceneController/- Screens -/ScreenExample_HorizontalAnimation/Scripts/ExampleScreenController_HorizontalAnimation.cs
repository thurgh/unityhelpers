using UnityHelpers.SceneController;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEngine;
using System;

public struct ExampleAnimatedScreenData_HorizontalAnimation
{
    public Action OnOpen;
    public Action OnClose;

    public ExampleAnimatedScreenData_HorizontalAnimation(Action onOpen = null, Action onClose = null)
    {
        OnOpen = onOpen;
        OnClose = onClose;
    }
}

public class ExampleScreenController_HorizontalAnimation : SceneController<ExampleAnimatedScreenData_HorizontalAnimation>
{
    public const string NAME_SCENE = "ScreenExample_HorizontalAnimation";

    [SerializeField] private Button backButton;

    public static void Show(ExampleAnimatedScreenData_HorizontalAnimation data)
    {
        Show(data, NAME_SCENE, LoadSceneMode.Additive);
    }

    public static void Hide()
    {
        Hide(NAME_SCENE);
    }

    public override void OnStart(ExampleAnimatedScreenData_HorizontalAnimation data)
    {
        backButton.onClick.AddListener(OnBackButton);
    }

    public override void OnOpen(ExampleAnimatedScreenData_HorizontalAnimation data)
    {
        data.OnOpen?.Invoke();
    }

    public override void OnClose(ExampleAnimatedScreenData_HorizontalAnimation data)
    {
        data.OnClose?.Invoke();
    }

    private void OnBackButton()
    {
        Hide();
    }
}