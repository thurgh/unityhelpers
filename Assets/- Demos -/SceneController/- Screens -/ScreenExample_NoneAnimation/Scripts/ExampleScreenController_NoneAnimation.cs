﻿using UnityHelpers.SceneController;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEngine;
using System;

public struct ExampleScreenData_NoneAnimation
{
    public Action OnOpen;
    public Action OnClose;

    public ExampleScreenData_NoneAnimation(Action onOpen = null, Action onClose = null)
    {
        OnOpen = onOpen;
        OnClose = onClose;
    }
}

public class ExampleScreenController_NoneAnimation : SceneController<ExampleScreenData_NoneAnimation>
{
    public const string NAME_SCENE = "ScreenExample_NoneAnimation";

    [SerializeField] private Button backButton;

    public static void Show(ExampleScreenData_NoneAnimation data)
    {
        Show(data, NAME_SCENE, LoadSceneMode.Additive);
    }

    public static void Hide()
    {
        Hide(NAME_SCENE);
    }

    public override void OnStart(ExampleScreenData_NoneAnimation data)
    {
        backButton.onClick.AddListener(OnBackButtonClick);
    }

    public override void OnOpen(ExampleScreenData_NoneAnimation data)
    {
        data.OnOpen?.Invoke();
    }

    public override void OnClose(ExampleScreenData_NoneAnimation data)
    {
        data.OnClose?.Invoke();
    }

    private void OnBackButtonClick()
    {
        Hide();
    }
}