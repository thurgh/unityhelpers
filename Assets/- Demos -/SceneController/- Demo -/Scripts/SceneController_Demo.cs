using UnityHelpers.SceneController;
using UnityEngine.SceneManagement;
using UnityHelpers.Extensions;
using UnityEngine.UI;
using UnityEngine;
using System;

public struct SceneControllerData_Demo
{
    public Action OnOpen;
    public Action OnClose;

    public SceneControllerData_Demo(Action onOpen = null, Action onClose = null)
    {
        OnOpen = onOpen;
        OnClose = onClose;
    }
}

public class SceneController_Demo : SceneController<SceneControllerData_Demo>
{
    public const string NAME_SCENE = "SceneController_Demo";

    [SerializeField] private Button backButton;

    [SerializeField] private Button screenControllerButton_NoneAnimation;
    [SerializeField] private Button screenControllerButton_HorizontalAnimation;
    [SerializeField] private Button screenControllerButton_ModalAnimation;

    [SerializeField] private Button loadingButton;
    [SerializeField] private Button notificationButton;
    [SerializeField] private Button questionButton;
    [SerializeField] private Button headerNotificationButton;


    public static void Show(SceneControllerData_Demo data = new SceneControllerData_Demo())
    {
        Show(data, NAME_SCENE, LoadSceneMode.Additive);
    }

    public static void Hide(Action OnClose = null)
    {
        data.OnClose += OnClose;
        Hide(NAME_SCENE);
    }

    public override void OnStart(SceneControllerData_Demo data)
    {
        backButton.onClick.AddListener(OnBackButtonClick);

        screenControllerButton_NoneAnimation.onClick.AddListener(ScreenControllerNoneAnimation);
        screenControllerButton_HorizontalAnimation.onClick.AddListener(ScreenControllerHorizontalAnimation);
        screenControllerButton_ModalAnimation.onClick.AddListener(ScreenControllerModalAnimation);

        loadingButton.onClick.AddListener(LoadingModal);
        notificationButton.onClick.AddListener(NotificationModal);
        questionButton.onClick.AddListener(QuestionModal);
        headerNotificationButton.onClick.AddListener(HeaderNotificationModal);
    }

    public override void OnOpen(SceneControllerData_Demo data)
    {
        data.OnOpen?.Invoke();
    }

    public override void OnClose(SceneControllerData_Demo data)
    {
        data.OnClose?.Invoke();
    }

    private void OnBackButtonClick()
    {
        Hide();
    }

    private void ScreenControllerNoneAnimation()
    {
        ExampleScreenController_NoneAnimation.Show(new ExampleScreenData_NoneAnimation(onOpen: () =>
        {
            Debug.Log("Abri a cena");
        },
        onClose: () =>
        {
            Debug.Log("Fechei a cena");
        }));
    }

    private void ScreenControllerHorizontalAnimation()
    {
        ExampleScreenController_HorizontalAnimation.Show(new ExampleAnimatedScreenData_HorizontalAnimation(onOpen: () =>
        {
            Debug.Log("Abri a cena");
        },
        onClose: () =>
        {
            Debug.Log("Fechei a cena");
        }));
    }

    private void ScreenControllerModalAnimation()
    {
        ExampleScreenController_ModalAnimation.Show(new ExampleAnimatedScreenData_ModalAnimation(onOpen: () =>
        {
            Debug.Log("Abri a cena");
        },
        onClose: () =>
        {
            Debug.Log("Fechei a cena");
        }));
    }

    private void LoadingModal()
    {
        LoadingModalController.Show(new LoadingModalData(onOpen: () =>
        {
            Debug.Log("Open");
            StartCoroutine(Extensions.WaitingAsync(3f, () => LoadingModalController.Hide()));
        },
        onClose: () => Debug.Log("Close")));
    }

    private void NotificationModal()
    {
        NotificationModalController.Show(new NotificationModalData("Loja", "Voce desbloqueou um item!",
        onOpen: () => Debug.Log("Open"),
        onConfirm: () => Debug.Log("Confirm"),
        onCloseConfirm: () => Debug.Log("ConfirmClose")));
    }

    private void QuestionModal()
    {
        QuestionModalController.Show(new QuestionModalData("Sair", "Deseja mesmo fazer isso?", "Sim", "N�o",
        onOpen: () => Debug.Log("Open"),
        onClose: () => Debug.Log("Close"),
        onYesButtonClick: () => Debug.Log("Affirmative"),
        onNoButtonClick: () => Debug.Log("Negative"),
        onCloseYesButtonClick: () => Debug.Log("Affirmative"),
        onCloseNoButtonClick: () => Debug.Log("Negative")));
    }

    private void HeaderNotificationModal()
    {
        HeaderNotificationModalController.Show(new HeaderNotificationModalData("Compra efetuada com sucesso!",
        onOpen: () => Debug.Log("Open"),
        onClose: () => Debug.Log("Close")));
    }
}