using UnityHelpers.SceneController;
using UnityEngine.SceneManagement;
using UnityHelpers.FadeManager;
using UnityEngine.UI;
using UnityEngine;
using System;

public struct FadeManagerData_Demo
{
    public Action OnOpen;
    public Action OnClose;

    public FadeManagerData_Demo(Action onOpen = null, Action onClose = null)
    {
        OnOpen = onOpen;
        OnClose = onClose;
    }
}

public class FadeManager_Demo : SceneController<FadeManagerData_Demo>
{
    [SerializeField] private Button fadeManagerButton;

    public const string NAME_SCENE = "FadeManager_Demo";

    [SerializeField] private Button backButton;

    public static void Show(FadeManagerData_Demo data = new FadeManagerData_Demo())
    {
        Show(data, NAME_SCENE, LoadSceneMode.Additive);
    }

    public static void Hide(Action OnClose = null)
    {
        data.OnClose += OnClose;
        Hide(NAME_SCENE);
    }

    public override void OnStart(FadeManagerData_Demo data)
    {
        backButton.onClick.AddListener(OnBackButtonClick);

        FadeManager.FadeIn();

        fadeManagerButton.onClick.AddListener(() =>
        {
            FadeManager.FadeOut(OnEndFadeOut: () =>
            {
                Hide();
            });
        });
    }

    public override void OnOpen(FadeManagerData_Demo data)
    {
        data.OnOpen?.Invoke();
    }

    public override void OnClose(FadeManagerData_Demo data)
    {
        data.OnClose?.Invoke();
    }

    private void OnBackButtonClick()
    {
        FadeManager.FadeOut(OnEndFadeOut: () =>
        {
            Hide();
        });
    }
}