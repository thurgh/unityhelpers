﻿using System.Collections;
using UnityEngine;
using TMPro;

public class ARTrackingController_Example : MonoBehaviour
{
    [SerializeField] private ArTrackingCodeController arTrackingCodeController;
    [SerializeField] private TextMeshProUGUI imageTrackedNameText;

    private bool canScan = false;

    // Utilizado para não deixar o usuário trackear antes de terminar o Fade e causar bugs.
    private IEnumerator Start()
    {
        canScan = false;
        yield return new WaitForSeconds(1.1f);
        canScan = true;
    }

    private void OnEnable()
    {
        arTrackingCodeController.OnScanCode += OnScanCode;
    }

    private void OnDisable()
    {
        arTrackingCodeController.OnScanCode -= OnScanCode;
    }

    public void OnScanCode(string posterName)
    {
        if (!canScan) return;
        canScan = false;

        imageTrackedNameText.text = $"Poster Name: {posterName}";
    }
}
