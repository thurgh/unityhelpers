using UnityHelpers.SceneController;
using System;

public struct ARTracking_DemoData
{
	public Action OnOpen;
	public Action OnClose;

	public ARTracking_DemoData(Action onOpen = null, Action onClose = null)
	{
		OnOpen = onOpen;
		OnClose = onClose;
	}
}

public class ARTrackingController_Demo : SceneController<ARTracking_DemoData>
{
	public const string NAME_SCENE = "ARTracking_Demo";

	public static void Show(ARTracking_DemoData data = new ARTracking_DemoData())
	{
		Show(data, NAME_SCENE);
	}

	public static void Hide(Action OnClose = null)
	{
		data.OnClose += OnClose;
		Hide(NAME_SCENE);
	}

	public override void OnStart(ARTracking_DemoData data)
	{

	}

	public override void OnOpen(ARTracking_DemoData data)
	{
		data.OnOpen?.Invoke();
	}

	public override void OnClose(ARTracking_DemoData data)
	{
		data.OnClose?.Invoke();
	}
}