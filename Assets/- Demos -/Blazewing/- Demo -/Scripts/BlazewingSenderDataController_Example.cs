using UnityEngine.UI;
using UnityEngine;
using Blazewing;
using TMPro;

public struct DataControllerStruct
{
    public string message;
    public int number;
}

public class BlazewingSenderDataController_Example : MonoBehaviour
{
    [Header("- DataController -")]
    [SerializeField] private Button dataControllerButton;
    [SerializeField] private TMP_InputField dataControllerInputFieldMessage;
    [SerializeField] private TMP_InputField dataControllerInputFieldNumber;

    private void Start()
    {
        dataControllerButton.onClick.AddListener(() =>
        {
            DataController.Add(new DataControllerStruct()
            {
                message = dataControllerInputFieldMessage.text,
                number = int.Parse(dataControllerInputFieldNumber.text)
            });

            BlazewingReceiverController_Demo.Show();
        });
    }
}