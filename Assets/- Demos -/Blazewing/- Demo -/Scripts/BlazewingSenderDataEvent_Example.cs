﻿using UnityEngine.UI;
using UnityEngine;
using Blazewing;
using TMPro;

public class BlazewingSenderDataEvent_Example : MonoBehaviour
{
    [Header("- DataEvent -")]
    [SerializeField] private Button dataEventButton;
    [SerializeField] private TMP_InputField dataEventInputFieldMessage;
    [SerializeField] private TMP_InputField dataEventInputFieldNumber;

    private void Start()
    {
        dataEventButton.onClick.AddListener(() =>
        {
            DataEvent.Notify(new DataEventStruct
            {
                message = dataEventInputFieldMessage.text,
                number = int.Parse(dataEventInputFieldNumber.text)
            });
        });
    }
}