using UnityEngine;
using Blazewing;
using TMPro;

public struct DataEventStruct
{
    public string message;
    public int number;
} 

public class BlazewingReceiverDataEvent_Example : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI receiverText;

    private void OnReceiveEvent(DataEventStruct parameters)
    {
        receiverText.text = $"Message: {parameters.message}\nNumber: {parameters.number}";
    }

    public void OnEnable()
    {
        DataEvent.Register<DataEventStruct>(OnReceiveEvent);
    }

    public void OnDisable()
    {
        DataEvent.Unregister<DataEventStruct>(OnReceiveEvent);
    }
}