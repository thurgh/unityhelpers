using UnityHelpers.SceneController;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEngine;
using System;

public struct BlazewingReceiverData_Demo
{
    public Action OnOpen;
    public Action OnClose;

    public BlazewingReceiverData_Demo(Action onOpen = null, Action onClose = null)
    {
        OnOpen = onOpen;
        OnClose = onClose;
    }
}

public class BlazewingReceiverController_Demo : SceneController<BlazewingReceiverData_Demo>
{
    public const string NAME_SCENE = "BlazewingReceiver_Demo";

    [SerializeField] private Button backButton;

    public static void Show(BlazewingReceiverData_Demo data = new BlazewingReceiverData_Demo())
    {
        Show(data, NAME_SCENE, LoadSceneMode.Additive);
    }

    public static void Hide(Action OnClose = null)
    {
        data.OnClose += OnClose;
        Hide(NAME_SCENE);
    }

    public override void OnStart(BlazewingReceiverData_Demo data)
    {
        backButton.onClick.AddListener(OnBackButtonClick);
    }

    public override void OnOpen(BlazewingReceiverData_Demo data)
    {
        data.OnOpen?.Invoke();
    }

    public override void OnClose(BlazewingReceiverData_Demo data)
    {
        data.OnClose?.Invoke();
    }

    private void OnBackButtonClick()
    {
        Hide();
    }
}