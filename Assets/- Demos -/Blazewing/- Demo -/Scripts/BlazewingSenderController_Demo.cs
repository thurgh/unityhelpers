using UnityHelpers.SceneController;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEngine;
using System;

public struct BlazewingSenderData_Demo
{
    public Action OnOpen;
    public Action OnClose;

    public BlazewingSenderData_Demo(Action onOpen = null, Action onClose = null)
    {
        OnOpen = onOpen;
        OnClose = onClose;
    }
}

public class BlazewingSenderController_Demo : SceneController<BlazewingSenderData_Demo>
{
    public const string NAME_SCENE = "BlazewingSender_Demo";

    [SerializeField] private Button backButton;

    public static void Show(BlazewingSenderData_Demo data = new BlazewingSenderData_Demo())
    {
        Show(data, NAME_SCENE, LoadSceneMode.Additive);
    }

    public static void Hide(Action OnClose = null)
    {
        data.OnClose += OnClose;
        Hide(NAME_SCENE);
    }

    public override void OnStart(BlazewingSenderData_Demo data)
    {
        backButton.onClick.AddListener(OnBackButtonClick);
    }

    public override void OnOpen(BlazewingSenderData_Demo data)
    {
        data.OnOpen?.Invoke();
    }

    public override void OnClose(BlazewingSenderData_Demo data)
    {
        data.OnClose?.Invoke();
    }

    private void OnBackButtonClick()
    {
        Hide();
    }
}