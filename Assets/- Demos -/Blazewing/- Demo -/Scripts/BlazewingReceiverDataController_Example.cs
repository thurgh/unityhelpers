using UnityEngine;
using Blazewing;
using TMPro;

public class BlazewingReceiverDataController_Example : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI messageText;

    private void Start()
    {
        DataControllerStruct dataToReceive = DataController.Get<DataControllerStruct>();
        messageText.text = $"Message: {dataToReceive.message}\nNumber: {dataToReceive.number}";
    }
}