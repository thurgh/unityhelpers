using UnityHelpers.SceneController;
using UnityEngine.SceneManagement;
using UnityHelpers.RayBlocker;
using UnityEngine.UI;
using UnityEngine;
using System;
using TMPro;

public struct RayBlockerData_Demo
{
    public Action OnOpen;
    public Action OnClose;

    public RayBlockerData_Demo(Action onOpen = null, Action onClose = null)
    {
        OnOpen = onOpen;
        OnClose = onClose;
    }
}

public class RayBlockerController_Demo : SceneController<RayBlockerData_Demo>
{
    public const string NAME_SCENE = "RayBlocker_Demo";

    [SerializeField] private Button backButton;
    [SerializeField] private Button testButton;
    [SerializeField] private TextMeshProUGUI valueText;
    [SerializeField] private TextMeshProUGUI blockedText;
    private int value;
    private bool isBlocked;

    public static void Show(RayBlockerData_Demo data = new RayBlockerData_Demo())
    {
        Show(data, NAME_SCENE, LoadSceneMode.Additive);
    }

    public static void Hide(Action OnClose = null)
    {
        data.OnClose += OnClose;
        Hide(NAME_SCENE);
    }

    public override void OnStart(RayBlockerData_Demo data)
    {
        backButton.onClick.AddListener(OnBackButtonClick);
        testButton.onClick.AddListener(OnTestButtonClick);
        RefreshRayblocker();
    }

    public override void OnOpen(RayBlockerData_Demo data)
    {
        data.OnOpen?.Invoke();
    }

    public override void OnClose(RayBlockerData_Demo data)
    {
        data.OnClose?.Invoke();
    }

    private void OnBackButtonClick()
    {
        Hide();
    }

    private void OnTestButtonClick()
    {
        value++;
        valueText.text = value.ToString();
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.E))
        {
            isBlocked = !isBlocked;
            RefreshRayblocker();
        }
    }

    private void RefreshRayblocker()
    {
        blockedText.text = isBlocked ? "Bloqueado" : "Desbloqueado";
        blockedText.color = isBlocked ? Color.red : Color.green;

        if(isBlocked) RayBlockerController.Open();
        else RayBlockerController.Close();
    }
}