using UnityHelpers.SceneController;
using UnityHelpers.FadeManager;
using UnityHelpers.Extensions;
using UnityEngine.UI;
using UnityEngine;
using System;

public struct DemoData_Demo
{
    public Action OnOpen;
    public Action OnClose;

    public DemoData_Demo(Action onOpen = null, Action onClose = null)
    {
        OnOpen = onOpen;
        OnClose = onClose;
    }
}

public class DemoController_Demo : SceneController<DemoData_Demo>
{
    public const string NAME_SCENE = "Main";

    [SerializeField] private RectTransform forceContent;

    [Header("Templates")]
    [SerializeField] private Button loadingDemoButton;
    [SerializeField] private Button splashDemoButton;
    [SerializeField] private Button stickersDemoButton;
    [SerializeField] private Button swipeDemoButton;
    [SerializeField] private Button wallpapersDemoButton;

    [Header("Packages")]
    [SerializeField] private Button scenesDemoButton;
    [SerializeField] private Button rayBlockerDemoButton;
    [SerializeField] private Button fadeDemoButton;
    [SerializeField] private Button blazewingDemoButton;
    [SerializeField] private Button arTrackingDemoButton;

    [Header("More Packages")]

    [Header("Tools")]
    [SerializeField] private Button setEaseDemoButton;

    public static void Show(DemoData_Demo data = new DemoData_Demo())
    {
        Show(data, NAME_SCENE);
    }

    public static void Hide(Action OnClose = null)
    {
        data.OnClose += OnClose;
        Hide(NAME_SCENE);
    }

    public override void OnStart(DemoData_Demo data)
    {
        FadeManager.FadeIn();

        blazewingDemoButton.onClick.AddListener(OnBlazewingDemoButtonClick);
        arTrackingDemoButton.onClick.AddListener(OnARTrackingDemoButton);
        fadeDemoButton.onClick.AddListener(OnFadeDemoButtonClick);
        scenesDemoButton.onClick.AddListener(OnScenesDemoButtonClick);
        splashDemoButton.onClick.AddListener(OnSplashDemoButtonClick);
        loadingDemoButton.onClick.AddListener(OnLoadingDemoButtonClick);
        stickersDemoButton.onClick.AddListener(OnStickersDemoButtonClick);
        wallpapersDemoButton.onClick.AddListener(OnWallpapersDemoButtonClick);
        swipeDemoButton.onClick.AddListener(OnSwipeDemoButtonClick);
        setEaseDemoButton.onClick.AddListener(OnSetEaseDemoButtonClick);
        rayBlockerDemoButton.onClick.AddListener(OnRayBlockerDemoButtonClick);

        Extensions.ForceUpdate(forceContent);
    }

    public override void OnOpen(DemoData_Demo data)
    {
        data.OnOpen?.Invoke();
    }

    public override void OnClose(DemoData_Demo data)
    {
        data.OnClose?.Invoke();
    }

    private void OnBlazewingDemoButtonClick()
    {
        BlazewingSenderController_Demo.Show();
    }

    private void OnARTrackingDemoButton()
    {
        ARTrackingController_Demo.Show();
    }

    private void OnFadeDemoButtonClick()
    {
        FadeManager.FadeOut(OnEndFadeOut: () =>
        {
            FadeManager_Demo.Show(new FadeManagerData_Demo(onClose: () => FadeManager.FadeIn ()));
        });
    }

    private void OnScenesDemoButtonClick()
    {
        SceneController_Demo.Show();
    }

    private void OnSplashDemoButtonClick()
    {
        SplashController.Show();
    }

    private void OnLoadingDemoButtonClick()
    {
        LoadingController_Demo.Show();
    }

    private void OnStickersDemoButtonClick()
    {
        StickersDownloadController_Demo.Show();
    }

    private void OnWallpapersDemoButtonClick()
    {
        WallpaperDownloadController_Demo.Show();
    }

    private void OnSwipeDemoButtonClick()
    {
        SwipeController.Show();
    }

    private void OnSetEaseDemoButtonClick()
    {
        SetEaseController.Show();
    }

    private void OnRayBlockerDemoButtonClick()
    {
        RayBlockerController_Demo.Show();
    }
}