using System.Collections.Generic;
using UnityHelpers.Extensions;
using UnityEngine.UI;
using DG.Tweening;
using UnityEngine;
using TMPro;

public enum EaseMode { In, InOut, Out }
public enum ScaleMode { Crescent, Decrescent }

public class SetEaseAnimate : MonoBehaviour
{
    [Header("- DoTween Ease Controller -")]
    [SerializeField] private TextMeshProUGUI initialText;
    [SerializeField] private RectTransform cardsContent;
    [SerializeField] private RectTransform forceContent;
    [SerializeField] private GameObject cardPrefab;

    [Header("- Settings -")]
    private EaseMode easeMode = EaseMode.Out;
    private ScaleMode scaleMode = ScaleMode.Crescent;
    private float time = 1.5f;

    [Header("- Settings -")]
    [SerializeField] private TMP_Dropdown easeModeDropdown;
    [SerializeField] private TMP_Dropdown scaleModeDropdown;
    [SerializeField] private Slider timeSlider;
    [SerializeField] private Button animateButton;
    [SerializeField] private Button openButton;
    [SerializeField] private Button closeButton;

    private List<GameObject> modalsInUI = new List<GameObject>();
    private List<Tween> tweens = new List<Tween>();
    private int modalsLength = 12;

    private void Start()
    {
        initialText.text = string.Empty;

        for (int i = 0; i < modalsLength; i++)
        {
            modalsInUI.Add(Instantiate(cardPrefab, cardsContent));
        }

        CreateDropdown(easeModeDropdown, typeof(EaseMode));
        CreateDropdown(scaleModeDropdown, typeof(ScaleMode));
        timeSlider.onValueChanged.AddListener(OnSliderValueChange);

        animateButton.onClick.AddListener(OnAnimateButtonClick);
        openButton.onClick.AddListener(OnOpenButtonClick);
        closeButton.onClick.AddListener(OnCloseButtonClick);

        timeSlider.value = 0.5f;
        OnOpenButtonClick();

        Extensions.ForceUpdate(forceContent);
    }

    private void OnOpenButtonClick()
    {
        easeModeDropdown.SetValueWithoutNotify(2);
        scaleModeDropdown.SetValueWithoutNotify(0);
        OnAnimateButtonClick();
    }

    private void OnCloseButtonClick()
    {
        easeModeDropdown.SetValueWithoutNotify(0);
        scaleModeDropdown.SetValueWithoutNotify(1);
        OnAnimateButtonClick();
    }

    private void OnSliderValueChange(float value)
    {
        time = Mathf.Lerp(0.3f, 3f, value);
    }

    //TODO: Adicionar no Extensions
    private void CreateDropdown(TMP_Dropdown dropdown, System.Type enumList)
    {
        dropdown.ClearOptions();

        List<string> options = new List<string>();
        foreach (var easeMode in System.Enum.GetValues(enumList))
        {
            options.Add(easeMode.ToString());
        }

        dropdown.AddOptions(options);
    }

    private void OnAnimateButtonClick()
    {
        List<Ease> easeList = GetNextEase();

        foreach (var tween in tweens)
        {
            tween.Kill();
        }

        tweens.Clear();

        for (int i = 0; i < modalsLength; i++)
        {
            if (scaleMode == ScaleMode.Crescent)
            {
                modalsInUI[i].transform.localScale = Vector3.zero;
                tweens.Add(modalsInUI[i].transform.DOScale(1, time).SetEase(easeList[i]));
            }
            else if (scaleMode == ScaleMode.Decrescent)
            {
                modalsInUI[i].transform.localScale = Vector3.one;
                tweens.Add(modalsInUI[i].transform.DOScale(0, time).SetEase(easeList[i]));
            }

            modalsInUI[i].GetComponentInChildren<TextMeshProUGUI>().text = easeList[i].ToString();
        }
    }

    private List<Ease> GetNextEase()
    {
        List<Ease> easeList = new List<Ease>();

        easeMode = (EaseMode)easeModeDropdown.value;
        scaleMode = (ScaleMode)scaleModeDropdown.value;

        switch (easeMode)
        {
            case EaseMode.In:
                easeList.Add(Ease.InBack);
                easeList.Add(Ease.InBounce);
                easeList.Add(Ease.InCirc);
                easeList.Add(Ease.InCubic);
                easeList.Add(Ease.InElastic);
                easeList.Add(Ease.InExpo);
                easeList.Add(Ease.InFlash);
                easeList.Add(Ease.InQuad);
                easeList.Add(Ease.InQuart);
                easeList.Add(Ease.InQuint);
                easeList.Add(Ease.InSine);
                easeList.Add(Ease.Unset);
                break;

            case EaseMode.InOut:
                easeList.Add(Ease.InOutBack);
                easeList.Add(Ease.InOutBounce);
                easeList.Add(Ease.InOutCirc);
                easeList.Add(Ease.InOutCubic);
                easeList.Add(Ease.InOutElastic);
                easeList.Add(Ease.InOutExpo);
                easeList.Add(Ease.InOutFlash);
                easeList.Add(Ease.InOutQuad);
                easeList.Add(Ease.InOutQuart);
                easeList.Add(Ease.InOutQuint);
                easeList.Add(Ease.InOutSine);
                easeList.Add(Ease.Unset);
                break;

            case EaseMode.Out:
                easeList.Add(Ease.OutBack);
                easeList.Add(Ease.OutBounce);
                easeList.Add(Ease.OutCirc);
                easeList.Add(Ease.OutCubic);
                easeList.Add(Ease.OutElastic);
                easeList.Add(Ease.OutExpo);
                easeList.Add(Ease.OutFlash);
                easeList.Add(Ease.OutQuad);
                easeList.Add(Ease.OutQuart);
                easeList.Add(Ease.OutQuint);
                easeList.Add(Ease.OutSine);
                easeList.Add(Ease.Unset);
                break;
        }

        return easeList;
    }
}