using UnityHelpers.SceneController;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEngine;
using System;

public struct SetEaseData
{
    public Action OnOpen;
    public Action OnClose;

    public SetEaseData(Action onOpen = null, Action onClose = null)
    {
        OnOpen = onOpen;
        OnClose = onClose;
    }
}

public class SetEaseController : SceneController<SetEaseData>
{
    public const string NAME_SCENE = "SetEase_Demo";

    [SerializeField] private Button backButton;

    public static void Show(SetEaseData data = new SetEaseData())
    {
        Show(data, NAME_SCENE, LoadSceneMode.Additive);
    }

    public static void Hide(Action OnClose = null)
    {
        data.OnClose += OnClose;
        Hide(NAME_SCENE);
    }

    public override void OnStart(SetEaseData data)
    {
        backButton.onClick.AddListener(OnBackButtonClick);
    }

    public override void OnOpen(SetEaseData data)
    {
        data.OnOpen?.Invoke();
    }

    public override void OnClose(SetEaseData data)
    {
        data.OnClose?.Invoke();
    }

    private void OnBackButtonClick()
    {
        Hide();
    }
}