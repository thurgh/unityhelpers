using UnityEngine;
using TMPro;

public class TaxCalculator : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI textPJ;
    [SerializeField] private TextMeshProUGUI textPF;

    [SerializeField] private float ganhos_da_steam_em_dolares;
    [SerializeField] private float valor_dolar_hoje = 5.41f;
    [SerializeField] private int pessoas_na_equipe = 1;

    private float porcentagem_descontada_da_steam = 30f;
    private float porcentagem_descontada_do_governo_americano = 20f; //Tem gente que fala 30% e tem gente que fala 15% a 20%
    private float porcentagem_descontada_do_governo_brasileiro_PF = 27.5f;
    private float porcentagem_descontada_do_governo_brasileiro_PJ = 15f;

    private void OnValidate()
    {
        // ----- Steam ----- //
        float ganhos_da_steam_em_reais = ganhos_da_steam_em_dolares * valor_dolar_hoje;

        // ----- Pessoa Juridica ----- //
        float lucro_reais_PJ = retorna_lucro_descontando_taxas_e_impostos_PJ(ganhos_da_steam_em_reais);
        float lucro_individual_reais_PJ = lucro_reais_PJ / (float)pessoas_na_equipe;
        float lucro_individual_mensal_reais_PJ = lucro_individual_reais_PJ / 12f;

        textPJ.text = "// Pessoa Juridica //\n" +
        $"\n" +
        $"<color=white>Valor Steam:</color> {ganhos_da_steam_em_reais.ToString("C")}\n" +
        $"\n" +
        $"<color=white>Lucro Recebido:</color> {lucro_reais_PJ.ToString("C")}\n" +
        $"<color=white>Lucro Individual:</color> {lucro_individual_reais_PJ.ToString("C")}\n" +
        $"<color=white>\"Salario\" em 12 meses:</color> {lucro_individual_mensal_reais_PJ.ToString("C")}\n";


        // ----- Pessoa F�sica ----- //
        float lucro_reais_PF = retorna_lucro_descontando_taxas_e_impostos_PF(ganhos_da_steam_em_reais);
        float lucro_individual_reais_PF = lucro_reais_PF / (float)pessoas_na_equipe;
        float lucro_individual_mensal_reais_PF = lucro_individual_reais_PF / 12f;

        textPF.text = "// Pessoa Fisica //\n" +
        $"\n" +
        $"<color=white>Valor Steam:</color> {ganhos_da_steam_em_reais.ToString("C")}\n" +
        $"\n" +
        $"<color=white>Lucro Recebido:</color> {lucro_reais_PF.ToString("C")}\n" +
        $"<color=white>Lucro Individual:</color> {lucro_individual_reais_PF.ToString("C")}\n" +
        $"<color=white>\"Salario\" em 12 meses:</color> {lucro_individual_mensal_reais_PF.ToString("C")}\n";
    }

    private float retorna_lucro_descontando_taxas_e_impostos_PJ(float ganhos_da_steam)
    {
        float valor_descontando_steam = ganhos_da_steam * (1f - (porcentagem_descontada_da_steam / 100f));

        float valor_descontando_governo_americano = valor_descontando_steam * (1f - (porcentagem_descontada_do_governo_americano / 100f));
        
        float valor_descontando_governo_brasileiro_PJ = valor_descontando_governo_americano * (1f - (porcentagem_descontada_do_governo_brasileiro_PJ / 100f));

        return valor_descontando_governo_brasileiro_PJ;
    }

    private float retorna_lucro_descontando_taxas_e_impostos_PF(float ganhos_da_steam)
    {
        float valor_descontando_steam = ganhos_da_steam * (1f - (porcentagem_descontada_da_steam / 100f));

        float valor_descontando_governo_americano = valor_descontando_steam * (1f - (porcentagem_descontada_do_governo_americano / 100f));

        float valor_descontando_governo_brasileiro_PF = valor_descontando_governo_americano * (1f - (porcentagem_descontada_do_governo_brasileiro_PF / 100f));

        return valor_descontando_governo_brasileiro_PF;
    }
}