using UnityEngine.UI;
using UnityEngine;
using System;

public struct WallpaperPrefab_Parameters
{
    public Wallpaper wallpaper;
    public Action<Wallpaper> OnDownloadButtonClick;
}

public class WallpaperPrefab : MonoBehaviour
{
    [SerializeField] private GameObject lockedIcon;
    [SerializeField] private Button downloadButton;
    [SerializeField] private Image wallpaperImage;

    public void Initialize(WallpaperPrefab_Parameters parameters)
    {
        downloadButton.onClick.AddListener(() => parameters.OnDownloadButtonClick(parameters.wallpaper));

        if (parameters.wallpaper.isUnlocked)
        {
            lockedIcon.SetActive(false);
            wallpaperImage.gameObject.SetActive(true);
            downloadButton.interactable = true;
            wallpaperImage.sprite = ConvertToSprite(parameters.wallpaper.texture);
        }
        else
        {
            lockedIcon.SetActive(true);
            wallpaperImage.gameObject.SetActive(false);
            downloadButton.interactable = false;
        }
    }

    private static Sprite ConvertToSprite(Texture2D texture)
    {
        return Sprite.Create(texture, new Rect(0, 0, texture.width, texture.height), Vector2.zero);
    }
}