using UnityHelpers.SceneController;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEngine;
using System;

public struct WallpapersDownloadData
{
    public Action OnOpen;
    public Action OnClose;

    public WallpapersDownloadData(Action onOpen = null, Action onClose = null)
    {
        OnOpen = onOpen;
        OnClose = onClose;
    }
}

public class WallpaperDownloadController_Demo : SceneController<WallpapersDownloadData>
{
    public const string NAME_SCENE = "WallpapersDownload_Demo";

    [Header("- Wallpapers Download Controller -")]
    [SerializeField] private AllWallpapersScriptable allWallpapersReward;
    [SerializeField] private WallpaperPrefab wallpaperPrefab;
    [SerializeField] private RectTransform content;
    [SerializeField] private Button backButton;
    [SerializeField] private Swipe swipe;

    private string folderName = "Wallpapers";

    public static void Show(WallpapersDownloadData data = new WallpapersDownloadData())
    {
        Show(data, NAME_SCENE, LoadSceneMode.Additive);
    }

    public static void Hide(Action OnClose = null)
    {
        data.OnClose += OnClose;
        Hide(NAME_SCENE);
    }

    public override void OnStart(WallpapersDownloadData data)
    {
        backButton.onClick.AddListener(OnBackButtonClick);
    }

    public override void OnOpen(WallpapersDownloadData data)
    {
        data.OnOpen?.Invoke();
    }

    public override void OnClose(WallpapersDownloadData data)
    {
        data.OnClose?.Invoke();
    }

    private void OnBackButtonClick()
    {
        Hide();
    }

    private void Start()
    {
        backButton.onClick.AddListener(OnBackButtonClick);

        for (int i = 0; i < allWallpapersReward.wallpapers.Count; i++)
        {
            Instantiate(wallpaperPrefab, content).Initialize(new WallpaperPrefab_Parameters
            {
                wallpaper = allWallpapersReward.wallpapers[i],
                OnDownloadButtonClick = OnDownloadWallpaperButtonClick
            });
        }
    }
    
    private void OnDownloadWallpaperButtonClick(Wallpaper wallpaper)
    {
        if (wallpaper.isUnlocked)
        {
            NotificationModalController.Show(new NotificationModalData("Wallpaper", "WALLPAPER BLOQUEADO."));
        }
        else
        {
            LoadingModalController.Show(new LoadingModalData(onOpen: () =>
            {
                SaveWallpaperOnDevice(folderName, wallpaper.texture);
            }));
        }
    }

    private void SaveWallpaperOnDevice(string folderName, Texture2D texture)
    {
        NativeGallery.SaveImageToGallery(texture, folderName, texture.name + ".jpg",
        (success, path) =>
        {
            LoadingModalController.Hide(onClose: () =>
            {
                if (success)
                {
                    NotificationModalController.Show(new NotificationModalData("Wallpaper", "WALLPAPER SALVO NA GALERIA COM SUCESSO."));
                }
                else
                {
                    NotificationModalController.Show(new NotificationModalData("Wallpaper", "ALGO DE ERRADO ACONTECEU AO SALVAR O WALLPAPER."));
                }
            });
        });
    }
}