﻿using UnityEngine;
using System;

[Serializable]
public class Wallpaper
{
    public bool isUnlocked = false;
    public Texture2D texture;
}