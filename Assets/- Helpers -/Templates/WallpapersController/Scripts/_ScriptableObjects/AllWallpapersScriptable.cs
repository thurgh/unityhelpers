﻿using System.Collections.Generic;
using UnityEngine;
using System;

[Serializable]
[CreateAssetMenu(menuName = "Rewards/Wallpaper/AllWallpapersReward")]
public class AllWallpapersScriptable : ScriptableObject
{
    public List<Wallpaper> wallpapers = new List<Wallpaper>();

    public List<Texture2D> GetUnlockedWallpapers()
    {
        List<Texture2D> unlockedWallpaper = new List<Texture2D>();

        for (int i = 0; i < wallpapers.Count; i++)
        {
            if (wallpapers[i].isUnlocked)
            {
                unlockedWallpaper.Add(wallpapers[i].texture);
            }
        }

        return unlockedWallpaper;
    }
}
