﻿using UnityHelpers.SceneController;
using UnityHelpers.FadeManager;
using System.Collections;
using DG.Tweening;
using UnityEngine;
using System;

public struct SplashData
{
    public Action OnOpen;
    public Action OnClose;

    public SplashData(Action onOpen = null, Action onClose = null)
    {
        OnOpen = onOpen;
        OnClose = onClose;
    }
}

public class SplashController : SceneController<SplashData>
{
    public const string NAME_SCENE = "Splash";

    [SerializeField] private Transform splash;


    public static void Show(SplashData data = new SplashData())
    {
        Show(data, NAME_SCENE);
    }

    public static void Hide(Action OnClose = null)
    {
        data.OnClose += OnClose;
        Hide(NAME_SCENE);
    }

    public override void OnStart(SplashData data)
    {
        StartCoroutine(SplashAnimation());
    }

    public override void OnOpen(SplashData data)
    {
        data.OnOpen?.Invoke();
    }

    public override void OnClose(SplashData data)
    {
        data.OnClose?.Invoke();
    }

    private IEnumerator SplashAnimation()
    {
        splash.DOScale(1.05f, 6f);

        FadeManager.SetFadeEnabledByState(true, FadeMode.BlackFade);

        FadeManager.FadeIn(duration: 1f, fadeMode: FadeMode.BlackFade, maskMode: MaskMode.None);

        yield return new WaitForSeconds(3f);

        FadeManager.FadeOut(duration: 1f, fadeMode: FadeMode.BlackFade, maskMode: MaskMode.None, OnEndFadeOut: () =>
        {
            GoToNextScene();
        });
    }

    private void GoToNextScene()
    {
        //MainMenuController.Show();
    }
}