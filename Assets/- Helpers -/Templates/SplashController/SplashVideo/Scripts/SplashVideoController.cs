using UnityHelpers.SceneController;
using UnityHelpers.FadeManager;
using UnityEngine.Video;
using DG.Tweening;
using UnityEngine;
using System;

public struct SplashVideoData
{
    public Action OnOpen;
    public Action OnClose;

    public SplashVideoData(Action onOpen = null, Action onClose = null)
    {
        OnOpen = onOpen;
        OnClose = onClose;
    }
}

public class SplashVideoController : SceneController<SplashVideoData>
{
    public const string NAME_SCENE = "Splash";

    [SerializeField] private VideoPlayer videoPlayer;

    public static void Show(SplashVideoData data = new SplashVideoData())
    {
        Show(data, NAME_SCENE);
    }

    public static void Hide(Action OnClose = null)
    {
        data.OnClose += OnClose;
        Hide(NAME_SCENE);
    }

    public override void OnOpen(SplashVideoData data)
    {
        data.OnOpen?.Invoke();
    }

    public override void OnClose(SplashVideoData data)
    {
        data.OnClose?.Invoke();
    }

    public override void OnStart(SplashVideoData data)
    {
        FadeManager.FadeIn();

        Application.targetFrameRate = 120;

        videoPlayer.loopPointReached += source =>
        {
            var t = DOTween.To(() => source.targetCameraAlpha, x => source.targetCameraAlpha = x, 0, 0.3f);
            t.SetTarget(source);
            t.OnComplete(() =>
            {
                FadeManager.FadeOut(duration: 1f, fadeMode: FadeMode.BlackFade, maskMode: MaskMode.None, OnEndFadeOut: () =>
                {
                    GoToNextScene();
                });
            });
        };
    }

    private void GoToNextScene()
    {
        //MainMenuController.Show();
    }

}