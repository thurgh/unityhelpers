﻿using UnityHelpers.SceneController;
using UnityEngine.SceneManagement;
using UnityHelpers.FadeManager;
using System.Collections;
using UnityEngine.UI;
using UnityEngine;

public struct LoadingData_Demo { }

public class LoadingController_Demo : SceneController<LoadingData_Demo>
{
    public const string NAME_SCENE = "Loading_Demo";

    [SerializeField] private Image slider;
    [SerializeField] private float loadingMinTime = 2f;

    private float timer = 0;

    public static void Show(LoadingData_Demo data = new LoadingData_Demo())
    {
        Show(data, NAME_SCENE);
    }

    public static void Hide()
    {
        Hide(NAME_SCENE);
    }

    public override void OnStart(LoadingData_Demo data)
    {
        slider.fillAmount = 0f;
        StartCoroutine(LoadScene(DemoController_Demo.NAME_SCENE));
    }

    public override void OnOpen(LoadingData_Demo data)
    {

    }

    public override void OnClose(LoadingData_Demo data)
    {

    }

    private IEnumerator FakeLoadScene()
    {
        FadeManager.FadeIn();

        while (timer < loadingMinTime)
        {
            timer += Time.deltaTime;
            slider.fillAmount = timer / loadingMinTime;
            yield return null;
        }

        slider.fillAmount = 1f;

        FadeManager.FadeOut(OnEndFadeOut: () =>
        {
            DemoController_Demo.Show();
        });
    }

    private IEnumerator LoadScene(string nextSceneName)
    {
        FadeManager.FadeIn();

        AsyncOperation async = SceneManager.LoadSceneAsync(nextSceneName);
        async.allowSceneActivation = false;

        while (async.progress < 0.9f || timer < loadingMinTime)
        {
            timer += Time.deltaTime;
            float timerProgress = timer / loadingMinTime;
            float loadingProgress = Mathf.Lerp(0f, Mathf.Clamp01(async.progress / 0.9f), timerProgress);
            slider.fillAmount = (loadingProgress + timerProgress) / 2f;
            yield return null;
        }

        slider.fillAmount = 1f;

        FadeManager.FadeOut(OnEndFadeOut: () =>
        {
            async.allowSceneActivation = true;
        });
    }
}