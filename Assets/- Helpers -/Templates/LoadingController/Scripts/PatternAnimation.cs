using UnityEngine.UI;
using UnityEngine;

public class PatternAnimation : MonoBehaviour
{
    [SerializeField] private RawImage pattern;
    [SerializeField] private float patternSpeed = 0.1f;
    [SerializeField] private Vector2 direction = new Vector2(1f, 1f);

    private void Update()
    {
        float speed = Time.deltaTime * patternSpeed;
        Vector2 newPos = pattern.uvRect.position + direction.normalized * speed;
        pattern.uvRect = new Rect(newPos, pattern.uvRect.size);
    }
}
