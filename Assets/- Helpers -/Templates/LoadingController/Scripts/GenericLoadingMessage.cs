using System.Collections;
using UnityEngine;
using TMPro;

public class GenericLoadingMessage : MonoBehaviour
{
    [SerializeField] private string loadingMessage = "Carregando";
    [SerializeField] private TextMeshProUGUI loadingText;

    private IEnumerator Start()
    {
        while (true)
        {
            loadingText.text = $"{loadingMessage}.";
            yield return new WaitForSeconds(1f);
            loadingText.text = $"{loadingMessage}..";
            yield return new WaitForSeconds(1f);
            loadingText.text = $"{loadingMessage}...";
            yield return new WaitForSeconds(1f);
        }
    }
}
