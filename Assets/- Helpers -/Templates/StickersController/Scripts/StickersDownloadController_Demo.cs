using UnityHelpers.SceneController;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEngine;
using System;

public struct StickersDownloadData_Demo
{
    public Action OnOpen;
    public Action OnClose;

    public StickersDownloadData_Demo(Action onOpen = null, Action onClose = null)
    {
        OnOpen = onOpen;
        OnClose = onClose;
    }
}

public class StickersDownloadController_Demo : SceneController<StickersDownloadData_Demo>
{
    public const string NAME_SCENE = "StickersDownload_Demo";

    [Header("- Stickers Download Controller -")]
    [SerializeField] private RectTransform content;
    [SerializeField] private Button backButton;
    [SerializeField] private TextAsset stickerPackage;
    [SerializeField] private StickersContentPrefab stickersContentPrefab;
    [SerializeField] private AllStickerPackageScriptable allRewards;

    public static void Show(StickersDownloadData_Demo data = new StickersDownloadData_Demo())
    {
        Show(data, NAME_SCENE, LoadSceneMode.Additive);
    }

    public static void Hide(Action OnClose = null)
    {
        data.OnClose += OnClose;
        Hide(NAME_SCENE);
    }

    public override void OnStart(StickersDownloadData_Demo data)
    {
        backButton.onClick.AddListener(OnBackButtonClick);

        foreach (var stickerPackage in allRewards.stickerPackages)
        {
            Instantiate(stickersContentPrefab, content).Initialize(new StickersContentPrefab_Parameters()
            {
                stickerPackageScriptable = stickerPackage,
                OnStickerDownloadButtonClick = OnStickerDownloadButtonClick
            });
        }
    }

    public override void OnOpen(StickersDownloadData_Demo data)
    {
        data.OnOpen?.Invoke();
    }

    public override void OnClose(StickersDownloadData_Demo data)
    {
        data.OnClose?.Invoke();
    }

    private void OnBackButtonClick()
    {
        Hide();
    }

    private void OnStickerDownloadButtonClick()
    {
        bool shareStickers = LigaStickerManager.ShareStickerPack(stickerPackage.text);

        if (shareStickers)
        {
            NotificationModalController.Show(new NotificationModalData("Sticker", "STICKER BAIXADO COM SUCESSO."));
        }
        else
        {
            NotificationModalController.Show(new NotificationModalData("Sticker", "Sticker packs n�o s�o suportados nessa plataforma."));
        }
    }
}