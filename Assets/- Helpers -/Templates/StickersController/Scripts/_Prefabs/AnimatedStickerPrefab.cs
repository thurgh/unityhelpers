﻿using UnityEngine.Video;
using UnityEngine.UI;
using UnityEngine;

public struct AnimatedStickerPrefab_Parameters
{
    public VideoClip videoClip;
}

public class AnimatedStickerPrefab : MonoBehaviour
{
    [SerializeField] private RawImage rawImage;
    [SerializeField] private VideoPlayer videoPlayer;
    [SerializeField] private Image lockedImage;

    public void Initialize(AnimatedStickerPrefab_Parameters parameters)
    {
        if (parameters.videoClip)
        {
            videoPlayer.gameObject.SetActive(true);
            Play(parameters.videoClip);
            lockedImage.enabled = false;
        }
        else
        {
            lockedImage.enabled = true;
            videoPlayer.gameObject.SetActive(false);
        }
    }

    public void Play(VideoClip clip, Texture2D texture2D = null)
    {
        var renderTexture = new RenderTexture(100, 100, 100);

        if (!texture2D)
        {
            Graphics.Blit(texture2D, renderTexture);
        }

        rawImage.texture = renderTexture;

        videoPlayer.clip = clip;
        videoPlayer.targetTexture = renderTexture;
        videoPlayer.isLooping = true;
        videoPlayer.Play();
    }
}