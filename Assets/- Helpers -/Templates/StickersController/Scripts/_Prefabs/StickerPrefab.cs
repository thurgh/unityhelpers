using UnityEngine.UI;
using UnityEngine;

public struct StickerPrefab_Parameters
{
    public Texture2D texture;
}

public class StickerPrefab : MonoBehaviour
{
    [SerializeField] private Image stickerImage;
    [SerializeField] private Image lockedImage;

    public void Initialize(StickerPrefab_Parameters parameters)
    {
        if (parameters.texture)
        {
            stickerImage.enabled = true;
            lockedImage.enabled = false;
            stickerImage.sprite = ConvertToSprite(parameters.texture);
        }
        else
        {
            lockedImage.enabled = true;
            stickerImage.enabled = false;
        }
    }

    public Sprite ConvertToSprite(Texture2D texture)
    {
        return Sprite.Create(texture, new Rect(0, 0, texture.width, texture.height), Vector2.zero);
    }
}