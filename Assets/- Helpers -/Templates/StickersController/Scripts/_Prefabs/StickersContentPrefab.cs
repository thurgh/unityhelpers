using System.Collections.Generic;
using UnityEngine.Video;
using UnityEngine.UI;
using UnityEngine;
using System;
using TMPro;

public struct StickersContentPrefab_Parameters
{
    public StickerPackageScriptable stickerPackageScriptable;
    public Action OnStickerDownloadButtonClick;
}

public class StickersContentPrefab : MonoBehaviour
{
    [SerializeField] private RectTransform content;
    [SerializeField] private TextMeshProUGUI remainingStickers;
    [SerializeField] private Button downloadButton;
    [SerializeField] private StickerPrefab staticStickerPrefab;
    [SerializeField] private AnimatedStickerPrefab AnimatedstickerPrefab;

    public void Initialize(StickersContentPrefab_Parameters parameters)
    {
        int staticStickersCount = parameters.stickerPackageScriptable.staticStickers.Count;
        List<Texture2D> unlockedStickers = parameters.stickerPackageScriptable.GetUnlockedStaticStickers();
        InstantiateStaticStickers(staticStickersCount, unlockedStickers, content);

        int animatedStickersCount = parameters.stickerPackageScriptable.animatedStickers.Count;
        List<VideoClip> unlockedAnimatedStickers = parameters.stickerPackageScriptable.GetUnlockedAnimatedStickers();
        InstantiateAnimatedStickers(animatedStickersCount, unlockedAnimatedStickers, content);

        int totalUnlockedStickersCount = unlockedStickers.Count + unlockedAnimatedStickers.Count;
        int totalStickersCount = staticStickersCount + animatedStickersCount;

        downloadButton.interactable = (totalUnlockedStickersCount == totalStickersCount);
        remainingStickers.gameObject.SetActive(!downloadButton.interactable);
        remainingStickers.text = $"Restam {totalStickersCount - totalUnlockedStickersCount} Stickers para desbloquear.";

        downloadButton.onClick.AddListener(() => parameters.OnStickerDownloadButtonClick());
    }

    private void InstantiateStaticStickers(int totalStaticStickersCount, List<Texture2D> unlockedStickers, RectTransform content)
    {
        for (int i = 0; i < totalStaticStickersCount; i++)
        {
            Texture2D unlockedStickersTexture = i < unlockedStickers.Count ? unlockedStickers[i] : null;
            StickerPrefab newStaticSticker = Instantiate(staticStickerPrefab, content);
            newStaticSticker.Initialize(new StickerPrefab_Parameters()
            {
                texture = unlockedStickersTexture
            });
        }
    }

    private void InstantiateAnimatedStickers(int totalAnimatedStickersCount, List<VideoClip> unlockedAnimatedStickers, RectTransform content)
    {
        for (int i = 0; i < totalAnimatedStickersCount; i++)
        {
            VideoClip unlockedAnimatedStickersTexture = i < unlockedAnimatedStickers.Count ? unlockedAnimatedStickers[i] : null;
            AnimatedStickerPrefab newAnimatedStickerPrefab = Instantiate(AnimatedstickerPrefab, content);
            newAnimatedStickerPrefab.Initialize(new AnimatedStickerPrefab_Parameters()
            {
                videoClip = unlockedAnimatedStickersTexture
            });
        }
    }
}