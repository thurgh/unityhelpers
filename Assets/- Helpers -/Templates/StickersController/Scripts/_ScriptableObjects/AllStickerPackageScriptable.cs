﻿using System.Collections.Generic;
using UnityEngine;
using System;

[Serializable]
[CreateAssetMenu(menuName = "Rewards/Stickers/AllStickersReward")]
public class AllStickerPackageScriptable : ScriptableObject
{
    public List<StickerPackageScriptable> stickerPackages = new List<StickerPackageScriptable>();
}