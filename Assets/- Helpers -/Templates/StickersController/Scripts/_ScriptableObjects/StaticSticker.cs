﻿using UnityEngine;
using System;

[Serializable]
public class StaticSticker
{
    public bool isUnlocked = false;
    public Texture2D staticSticker;
}