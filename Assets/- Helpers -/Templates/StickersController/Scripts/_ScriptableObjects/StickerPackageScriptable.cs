﻿using System.Collections.Generic;
using UnityEngine.Video;
using UnityEngine;
using System;

[Serializable]
[CreateAssetMenu(menuName = "Rewards/Stickers/StickerPackageScriptable")]
public class StickerPackageScriptable : ScriptableObject
{
    public TextAsset stickerPackageFile;
    public List<StaticSticker> staticStickers = new List<StaticSticker>();
    public List<AnimatedSticker> animatedStickers = new List<AnimatedSticker>();

    public List<Texture2D> GetUnlockedStaticStickers()
    {
        List<Texture2D> unlockedStickers = new List<Texture2D>();

        for (int i = 0; i < staticStickers.Count; i++)
        {
            if (staticStickers[i].isUnlocked)
            {
                unlockedStickers.Add(staticStickers[i].staticSticker);
            }
        }

        return unlockedStickers;
    }

    public List<VideoClip> GetUnlockedAnimatedStickers()
    {
        List<VideoClip> unlockedStickers = new List<VideoClip>();

        for (int i = 0; i < animatedStickers.Count; i++)
        {
            if (animatedStickers[i].isUnlocked)
            {
                unlockedStickers.Add(animatedStickers[i].animatedSticker);
            }
        }

        return unlockedStickers;
    }
}