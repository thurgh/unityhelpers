﻿using UnityEngine.Video;
using System;

[Serializable]
public class AnimatedSticker
{
    public bool isUnlocked = false;
    public VideoClip animatedSticker;
}
