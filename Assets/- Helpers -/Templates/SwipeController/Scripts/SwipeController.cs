using UnityHelpers.SceneController;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEngine;
using System;

public struct SwipeData
{
    public Action OnOpen;
    public Action OnClose;

    public SwipeData(Action onOpen = null, Action onClose = null)
    {
        OnOpen = onOpen;
        OnClose = onClose;
    }
}

public class SwipeController : SceneController<SwipeData>
{
    public const string NAME_SCENE = "Swipe_Demo";

    [SerializeField] private Button backButton;

    public static void Show(SwipeData data = new SwipeData())
    {
        Show(data, NAME_SCENE, LoadSceneMode.Additive);
    }

    public static void Hide(Action OnClose = null)
    {
        data.OnClose += OnClose;
        Hide(NAME_SCENE);
    }

    public override void OnStart(SwipeData data)
    {
        backButton.onClick.AddListener(OnBackButtonClick);
    }

    public override void OnOpen(SwipeData data)
    {
        data.OnOpen?.Invoke();
    }

    public override void OnClose(SwipeData data)
    {
        data.OnClose?.Invoke();
    }

    private void OnBackButtonClick()
    {
        Hide();
    }
}