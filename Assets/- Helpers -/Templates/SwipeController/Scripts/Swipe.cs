using Sirenix.OdinInspector;
using UnityEngine.UI;
using UnityEngine;
using DG.Tweening;

public class Swipe : MonoBehaviour
{
    [BoxGroup("Screens"), SerializeField] private int initialScreenIndex;

    [BoxGroup("Scroll"), SerializeField] private CanvasScaler canvas;
    [BoxGroup("Scroll"), SerializeField] private ScrollRect scrollRect;

    [BoxGroup("Buttons - Optional"), SerializeField] private Button nextButton;
    [BoxGroup("Buttons - Optional"), SerializeField] private Button previousButton;

    private int currentScreenIndex;
    private Vector2 lastValue;
    private float scrollSpeed;
    private int screensCount;
    private Tween swipeTween;

    public int ScreenIndex
    {
        get => currentScreenIndex;
        private set
        {
            if (value < 0)
            {
                currentScreenIndex = 0;
            }
            else if (value > screensCount - 1)
            {
                currentScreenIndex = screensCount - 1;
            }
            else
            {
                currentScreenIndex = value;
            }
        }
    }

    private void Start()
    {
        scrollRect.onValueChanged.AddListener(OnValueChanged);
        nextButton.onClick.AddListener(OnNextButtonClick);
        previousButton.onClick.AddListener(OnPreviousButtonClick);

        Initialize();
    }

    public void Initialize()
    {
        InitialScreen();
        ShowArrows();
    }

    private void OnNextButtonClick()
    {
        ScreenIndex++;
        HideArrows();
        SwipeAnimation();
    }

    private void OnPreviousButtonClick()
    {
        ScreenIndex--;
        HideArrows();
        SwipeAnimation();
    }

    private void OnValueChanged(Vector2 value)
    {
        scrollSpeed = (value.x - lastValue.x) * 100f;
        var currentSwipeValue = value.x;
        lastValue = value;

        ScreenIndex = Mathf.RoundToInt(currentSwipeValue * (screensCount - 1));
    }

    private void SwipeAnimation()
    {
        HideArrows();

        var targetSwipeValue = (float) ScreenIndex / (screensCount - 1);
        swipeTween = scrollRect.DOHorizontalNormalizedPos(targetSwipeValue, 0.3f).OnComplete(ShowArrows);
    }

    private void LateUpdate()
    {
        if (Input.GetAxisRaw("Mouse ScrollWheel") > 0f)
        {
            OnPreviousButtonClick();
        }
        else if (Input.GetAxisRaw("Mouse ScrollWheel") < 0f)
        {
            OnNextButtonClick();
        }

        if (Input.GetMouseButtonDown(0))
        {
            swipeTween.Kill();
            ShowArrows();
        }

        if (Input.GetMouseButtonUp(0))
        {
            var targetSwipeValue = (float)ScreenIndex / (screensCount - 1);
            float difference = Mathf.Abs(targetSwipeValue - scrollRect.horizontalNormalizedPosition);

            if(difference > 0.005f)
            {
                const float limitSpeed = 0.2f;

                if (scrollSpeed > limitSpeed)
                {
                    ScreenIndex++;
                    SwipeAnimation();
                }
                else if (scrollSpeed < -limitSpeed)
                {
                    ScreenIndex--;
                    SwipeAnimation();
                }
                else
                {
                    SwipeAnimation();
                }
            }
        }
    }

    private void HideArrows()
    {
        if (nextButton && previousButton)
        {
            previousButton.gameObject.SetActive(false);
            nextButton.gameObject.SetActive(false);
        }
    }

    private void ShowArrows()
    {
        if (nextButton && previousButton)
        {
            if (ScreenIndex == 0)
            {
                previousButton.gameObject.SetActive(false);
                nextButton.gameObject.SetActive(true);
            }
            else if (ScreenIndex == screensCount - 1)
            {
                previousButton.gameObject.SetActive(true);
                nextButton.gameObject.SetActive(false);
            }
            else
            {
                previousButton.gameObject.SetActive(true);
                nextButton.gameObject.SetActive(true);
            }
        }
    }

    [HorizontalGroup("Tools"), Button]
    public void InitialScreen()
    {
        screensCount = scrollRect.content.childCount;

        ScreenIndex = initialScreenIndex;
        float targetSwipeValue = (float)ScreenIndex / (screensCount - 1);
        scrollRect.horizontalNormalizedPosition = targetSwipeValue;
    }

    [HorizontalGroup("Tools"), Button]
    public void PreviousScreen()
    {
        screensCount = scrollRect.content.childCount;

        ScreenIndex--;
        float targetSwipeValue = (float)ScreenIndex / (screensCount - 1);
        scrollRect.horizontalNormalizedPosition = targetSwipeValue;
    }

    [HorizontalGroup("Tools"), Button]
    public void NextScreen()
    {
        screensCount = scrollRect.content.childCount;

        ScreenIndex++;
        float targetSwipeValue = (float)ScreenIndex / (screensCount - 1);
        scrollRect.horizontalNormalizedPosition = targetSwipeValue;
    }
}