# Unity Helpers - Documentação

<br>

## Progresso de Atualização:

##### Lembrete: v3 do Helpers em andamento pós MMO.

##### Packages (ok)
AR Tracking (equal)
Audio Manager (att)
BaseModals (att)
BaseUI (att)
Extensions (att)
FadeManager (equal)
InitializeProject (equal)
RayBlocker (equal)
RegisterVerifier (equal)
SceneController / UIController (equal)
StickerManager (equal)
VersionController (equal)

##### Data (ok)
* Rainbow Folders (equal)

##### Template
* LoadingController (equal)
* SplashController (equal)
* StickersController (equal)
* SwipeController (equal)
* WallpapersController (equal)

##### Tools (ok)
* SetEase (equal)
* Snippets (att)
* TaxCalculator (equal)

## Progresso de Adição:
* Blazewing (added)
* Splash

## Progresso de Documentação:
* Padronização de Audios e Variantes

<br>

## Geral
* Lembre-se que todos esses Helpers possuem exemplos de como serem utilizados.
* Link do repositório do projeto com os exemplos: https://gitlab.com/thurgh/unityhelpers
* Versão do Projeto: 2021.3 LTS

<br><br><br><br>

## RainbowFolderData
##### Funcionalidades:
* É um save do **'Data'** do **'RainbowFolder'** para não ter que ficar reconfigurando as cores das pastar a cada projeto novo.
#####  Como Usar:
* Vá em **'Assets\Plugins\RainbowAssets\RainbowFolders\Editor'** e substitua a pasta **'Data'**.
#####  Dependências:
* Rainbow Folder (Asset)

<br><br><br><br>

## ARTracking
##### Funcionalidades:
* Utilizado para retornar o nome de uma imagem ao trackear utilizando o AR Foundation.
#####  Como Usar:
* Referencie o Script **'ArTrackingCodeController'** e use a função **'OnScanCode'** para receber o nome da imagem quando trackeada.
#####  Dependências:
* AR Foundation (Asset)
#####  Exemplos:
```c#
private void OnEnabled()
{
    arTrackingCodeController.OnScanCode += OnScanCode;
}

public void OnScanCode(string posterName)
{
    Debug.Log($"Poster Name: {posterName}");
}
```

<br><br><br><br>

## InitializeProject
##### Funcionalidades:
* A ideia desse Helper é facilitar o uso de Managers em diversas cenas.
* Ele é uma Prefab que possui um Script que é o primeiro a ser ativado no projeto. Ao ser ativado ele Instância todos os Managers do projeto e ativa seus **'Initialize()'** para já configura-los antes de qualquer outra coisa.
* Caso precise testar uma outra cena sem passar pela cena inicial, basta adicionar esta prefab e pronto.
#####  Como Usar:
* Adicione a Prefab **'- Initialize Project -'** na cena e dentro dela adicione todas as Prefabs dos Managers na lista.
#####  Dependências:
* OdinInspector (Asset)

<br><br><br><br>

## FadeManager
##### Funcionalidades:
* Utilizado para aplicar FadeIn, FadeOut e fazer mudança entre cenas com Fade.
* Para facilitar o uso de cenas dentro do projeto. Ao invés de utilizarmos o nome da cena, utilizamos a classe **'SceneName'** que guarda o nome de todas as cenas em variáveis **'Const'**.
##### Como Usar:
* Adicione a Prefab **'- InitializeProject -'** na cena contendo a Prefab **'- FadeManager -'** dentro da lista de Managers.
* Abra a classe **'SceneName'** e crie nela uma variável 'const' com o nome da cena para cada cena a ser utilizada no projeto.
* Agora basta utilizar as funções **'FadeManager.LoadScene()'**, **'FadeManager.FadeIn()'**, **'FadeManager.FadeOut()'**.
##### Dependências:
* DoTween (Asset)
* InitializeProject (Helper)
#####  Exemplos:
```c#
FadeManager.LoadScene(SceneName.ExampleScene);

FadeManager.FadeIn();

FadeManager.FadeOut();
```

<br><br><br><br>

## ModalManager
##### Funcionalidades:
* Utilizado para abrir modais de forma fácil em cenas aditivas já com animação de abrir e fechar.
* Para facilitar o uso de cenas dentro do projeto. Ao invés de utilizarmos o nome da cena, utilizamos a classe **'ModalName'** que guarda o nome de todas as cenas de modais em variáveis **'Const'**.
* Caso haja mais de uma modal na tela, o próprio código cuida de ajustar o Order das modais para que sempre fique uma modal na frente da outra corretamente.
* A cena **'Modal_Background'** é usada para ficar como fade atrás da modal.
* Toda cena de modal deve ter **'Modal_'** no nome como padrão.
#####  Como Usar:
* Adicione a Prefab **'- InitializeProject -'** na cena contendo a Prefab **'- ModalManager -'** dentro da lista de Managers.
* Abra a classe **'ModalName'** e crie nela uma variável **'const'** com o nome da cena de modal para cada cena de modal a ser utilizada no projeto.
* Duplique a cena **'Modal_Example'** para acelerar o processo e coloque o nome que desejar na cena.
* Dentro dessa cena é necessário ter um **'ModalUI'** e um **'ModalController'**. Ou alguma classe que herde de **'ModalController'** caso necessário haver funções especificas dessa modal.
* Agora basta utilizar as funções **'ModalManager.OpenModal()'**, **'ModalManager.CloseModal()'**.
##### Melhorias
* Animar a cena **'Modal_Background'** para que tenha um fade mais suave.
#####  Dependências:
* Odin Inspector (Asset)
* InitializeProject (Helper)
#####  Exemplos:
```c#
ModalManager.OpenModal(ModalName.Modal_Example);

ModalManager.CloseModal(ModalName.Modal_Example);
```

<br><br><br><br>

## ModalManager - Notification
##### Funcionalidades:
* É um adicional do **'ModalManager'**, vem uma cena chamada **'Modal_Notification'**. Ela é uma modal simples que apenas mostra algum texto na tela e possui um botão de confirmar.
#####  Como Usar:
* Lembre-se de configurar a cena dentro do **'ModalManager'**.
* Primeiramente adicione um 'DataController' com um **'Notification'** de parâmetro contendo a mensagem a ser exibida na tela.
* Após isso basta abrir a modal com **'ModalManager.OpenModal(ModalName.Modal_Notification);'**.
#####  Dependências:
* ModalManager (Helper)
* Blazewing (Helper)
#####  Exemplos:
```c#
DataController.Add(new Notification()
{
    message = "Mensagem a ser exibida na modal."
});

ModalManager.OpenModal(ModalName.Modal_Notification);
```

<br><br><br><br>

# ModalManager - Loading
##### Funcionalidades:
* É um adicional do **'ModalManager'**, vem uma cena chamada **'Modal_Loading'**. Ela é uma modal simples que é usada em momentos de loading quando temos que esperar alguma resposta da API ou algo do tipo.
#####  Como Usar:
* Lembre-se de configurar a cena dentro do **'ModalManager'**.
* Basta abrir a cena com **'ModalManager.OpenModal(ModalName.Modal_Loading);'**.
#####  Dependências:
* ModalManager (Helper)
#####  Exemplos:
```c#
ModalManager.OpenModal(ModalName.Modal_Loading);

ModalManager.CloseModal(ModalName.Modal_Loading);
```

<br><br><br><br>

## RaycastCanvasManager
##### Funcionalidades:
* Esse Helper serve para bloquear a intereção de tela. Ele é usado durante uma transição por exemplo, para que o usuário não clique em nada enquando espera a transição.
#####  Como Usar:
* Adicione a Prefab **'- InitializeProject -'** na cena contendo a Prefab **'- RaycastCanvasManager -'** dentro da lista de Managers.
* Basta usar **'RaycastCanvasManager.OpenRaycastCanvas()'** para bloquear as interações e **'RaycastCanvasManager.CloseRaycastCanvas()'** para poder interagir novamente.
#####  Dependências:
* InitializeProject (Helper)
#####  Exemplos:
```c#
RaycastCanvasManager.OpenRaycastCanvas();

RaycastCanvasManager.CloseRaycastCanvas();
```

<br><br><br><br>

## SplashController
##### Funcionalidades:
* Como quase todo projeto tem uma splash inicial, esse Helper contém uma cena de Splash já pronta para uso.
#####  Como Usar:
* Duplique a cena **'Splash_Example'**, e configure o que precisar no Script **'SplashController'**.
#####  Dependências:
* FadeManager (Helper)

<br><br><br><br>

## Blazewing (DataController)
##### Funcionalidades:
* Criado por **Rafael Lazarin**, ele é utilizado para enviar dados para outras cenas.
#####  Como Usar:
* Crie uma Struct e depois utilize o **'DataController.Add(new DataControllerStruct { });'** para salvar a informação.
* Agora quando quiser receber as informações salvas, use **'DataController.Get<DataControllerStruct>();'**.
#####  Exemplos:
```c#
// Send Data
DataController.Add(new DataControllerStruct
{
    variable = value
});

//Receive Data
DataControllerStruct data = DataController.Get<DataControllerStruct>();
```

<br><br><br><br>

## Blazewing (DataEvent)
##### Funcionalidade:
* Criado por **Rafael Lazarin**, ele funciona como os **'EventListener'** que usávamos no **'ScriptableObjects-Architecture'** porém totalmente via código, sem precisa usar o inspector. Basicamente usamos ele para chamar e ouvir Eventos sem precisar linkar um script ao outro.
#####  Como Usar:
* Crie uma Struct e depois utilize o **'DataEvent.Notify(new DataEventStruct { });'** para chamar o evento.
* Agora para que os outros Scripts ouvirem esse evento, voce deve registrar essa Struct à alguma função usando **'DataEvent.Register<DataEventStruct>(OnReceiveEvent);'**.
* Dessa forma sempre que for chamado esse Evento, todos os Scripts registrados irão ativar a função desejada.
#####  Exemplos:
```c#
// Send Data
DataEvent.Notify(new DataEventStruct
{
    variable = value
});

//Receive Data
public void OnEnabled()
{
    DataEvent.Register<DataEventStruct>(OnReceiveEvent);
}

private void OnReceiveEvent(DataEventStruct parameters)
{
    receiverText.text = $"Message: {parameters.value}";
}
```