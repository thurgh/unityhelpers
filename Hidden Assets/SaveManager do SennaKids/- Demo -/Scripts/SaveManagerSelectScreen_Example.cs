using UnityEngine.UI;
using UnityEngine;

public enum SaveManagerScreen { Login, Register, SaveGameLoop }

public class SaveManagerSelectScreen_Example : MonoBehaviour
{
    public static SaveManagerSelectScreen_Example instance;

    [Header("- Screens -")]
    [SerializeField] private GameObject loginScreen;
    [SerializeField] private GameObject registerScreen;
    [SerializeField] private GameObject saveGameLoopScreen;

    [Header("- Buttons -")]
    [SerializeField] private Button loginScreenButton;
    [SerializeField] private Button registerScreenButton;
    [SerializeField] private Button saveGameLoopScreenButton;

    private void Awake()
    {
        instance = this;
    }

    private void Start()
    {
        loginScreenButton.onClick.AddListener(() => SelectScreen(SaveManagerScreen.Login));
        registerScreenButton.onClick.AddListener(() => SelectScreen(SaveManagerScreen.Register));
        saveGameLoopScreenButton.onClick.AddListener(() => SelectScreen(SaveManagerScreen.SaveGameLoop));

        SelectScreen(SaveManagerScreen.Login);
    }

    public static void SelectScreen(SaveManagerScreen screenToOpen)
    {
        instance.loginScreen.SetActive(false);
        instance.registerScreen.SetActive(false);
        instance.saveGameLoopScreen.SetActive(false);

        instance.loginScreenButton.interactable = true;
        instance.registerScreenButton.interactable = true;
        instance.saveGameLoopScreenButton.interactable = true;

        switch (screenToOpen)
        {
            case SaveManagerScreen.Login:
                instance.loginScreen.SetActive(true);
                instance.loginScreenButton.interactable = false;
                break;
            case SaveManagerScreen.Register:
                instance.registerScreen.SetActive(true);
                instance.registerScreenButton.interactable = false;
                break;
            case SaveManagerScreen.SaveGameLoop:
                instance.saveGameLoopScreen.SetActive(true);
                instance.saveGameLoopScreenButton.interactable = false;
                break;
        }
    }
}