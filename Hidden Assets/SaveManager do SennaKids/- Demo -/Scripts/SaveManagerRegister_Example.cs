using UnityEngine.UI;
using UnityEngine;
using TMPro;

public class SaveManagerRegister_Example : MonoBehaviour
{
    [Header("- Register -")]
    [SerializeField] private TMP_InputField nameRegisterInputField;
    [SerializeField] private TMP_InputField inepRegisterInputField;
    [SerializeField] private Button registerButton;

    private void Start()
    {
        registerButton.onClick.AddListener(() => OnRegisterButtonClick(inepRegisterInputField.text, nameRegisterInputField.text));
    }

    private void OnRegisterButtonClick(string inep, string nome)
    {
        ModalManager.OpenModal(ModalName.Modal_Loading, OnComplete: () =>
        {
            SaveManager.instance.OnMainRegister(inep, nome, OnSuccess: () =>
            {
                ModalManager.CloseModal(ModalName.Modal_Loading, OnCloseModal: () =>
                {
                    SaveManagerConsole_Example.ShowConsole("Sucesso ao registrar o usuario");
                    SaveManagerSelectScreen_Example.SelectScreen(SaveManagerScreen.Login);
                });
            },
            OnError: error =>
            {
                ModalManager.CloseModal(ModalName.Modal_Loading, OnCloseModal: () =>
                {
                    SaveManagerConsole_Example.ShowConsole($"Error: {error.message}");
                });
            });
        });
    }
}