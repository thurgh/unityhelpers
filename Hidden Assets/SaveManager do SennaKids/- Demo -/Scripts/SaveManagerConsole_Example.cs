using UnityEngine;
using TMPro;

public class SaveManagerConsole_Example : MonoBehaviour
{
    public static SaveManagerConsole_Example instance;
    public TextMeshProUGUI messageText;

    private void Awake()
    {
        instance = this;
    }

    public static void ShowConsole(string message)
    {
        instance.messageText.text = message;
    }
}