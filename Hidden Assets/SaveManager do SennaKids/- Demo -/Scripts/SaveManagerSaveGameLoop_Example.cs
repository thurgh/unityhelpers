using UnityEngine.UI;
using UnityEngine;
using TMPro;

public class SaveManagerSaveGameLoop_Example : MonoBehaviour
{
    [Header("- SaveGameLoop -")]
    [SerializeField] private TextMeshProUGUI payloadsText;
    [SerializeField] private Button saveLoopButton;
    [SerializeField] private Button winMedalButton;
    [SerializeField] private Button localSaveDeleteButton;

    private void Start()
    {
        saveLoopButton.onClick.AddListener(() => OnSaveLoopButtonClick());
        winMedalButton.onClick.AddListener(() => OnWinMedalButtonClick());
        localSaveDeleteButton.onClick.AddListener(() => OnLocalSaveDeleteButtonClick());

        RefreshPayloadsText();
    }

    private void OnSaveLoopButtonClick()
    {
        ModalManager.OpenModal(ModalName.Modal_Loading, OnComplete: () =>
        {
            SaveManager.instance.OnSaveGameLoop(OnSuccess: () =>
            {
                ModalManager.CloseModal(ModalName.Modal_Loading, OnCloseModal: () =>
                {
                    RefreshPayloadsText();
                });
            });
        });
    }

    private void OnWinMedalButtonClick()
    {
        string medalRandomName = Random.Range(1, 100).ToString();

        SaveManager.instance.data.gameDataToCloud.AddPayload(new MedalsPayload
        {
            name = $"Name_{medalRandomName}",
            description = $"Description_{medalRandomName}",
        });

        RefreshPayloadsText();
    }

    private void OnLocalSaveDeleteButtonClick()
    {
        SaveManager.instance.DeleteAllFilesOfAllUsers();
    }

    private void RefreshPayloadsText()
    {
        payloadsText.text = $"Numero de Payloads: {SaveManager.instance.data.gameDataToCloud.payloads.Count}";
    }
}