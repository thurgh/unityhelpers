using UnityEngine.UI;
using UnityEngine;
using TMPro;
using API;

public class SaveManagerLogin_Example : MonoBehaviour
{
    [Header("- Login -")]
    [SerializeField] private TMP_InputField inepLoginInputField;
    [SerializeField] private Button loginButton;

    private void Start()
    {
        loginButton.onClick.AddListener(() => OnLoginButtonClick(inepLoginInputField.text));
    }

    private void OnLoginButtonClick(string inep)
    {
        //inepRegisterInputField.text = inep;
        ModalManager.OpenModal(ModalName.Modal_Loading, OnComplete: ()=>
        {
            SaveManager.instance.OnMainLogin(inep, OnLoginApiSuccess: () =>
            {
                ModalManager.CloseModal(ModalName.Modal_Loading, OnCloseModal: () =>
                {
                    SaveManagerConsole_Example.ShowConsole("Usuario logado na API com sucesso");
                    SaveManagerSelectScreen_Example.SelectScreen(SaveManagerScreen.Login);
                });
            },
            OnErrorWithLocalLogin: error =>
            {
                ModalManager.CloseModal(ModalName.Modal_Loading, OnCloseModal: () =>
                {
                    SaveManagerConsole_Example.ShowConsole("Erro ao logar na API. Por�m usuario logado Localmente com sucesso");
                    SaveManagerSelectScreen_Example.SelectScreen(SaveManagerScreen.SaveGameLoop);
                });
            },
            OnError: error =>
            {
                if (error.statusCode == StatusCode.Unauthorized)
                {
                    ModalManager.CloseModal(ModalName.Modal_Loading, OnCloseModal: () =>
                    {
                        SaveManagerConsole_Example.ShowConsole("Esse usuario n�o existe");
                        SaveManagerSelectScreen_Example.SelectScreen(SaveManagerScreen.Register);
                    });
                }
                else
                {
                    ModalManager.CloseModal(ModalName.Modal_Loading, OnCloseModal: () =>
                    {
                        SaveManagerConsole_Example.ShowConsole($"Error: {error.message}");
                    });
                }
            });
        });
    }
}