﻿using System.Collections.Generic;
using System;

[Serializable]
public class UserProxy
{
    // Dados do usuario
    public int id;
    public string inep;
    public string name;

    // Exemplo de dados do jogo
    public float volume;
    public int version;
    public List<MedalsProxy> medals = new List<MedalsProxy>();
}