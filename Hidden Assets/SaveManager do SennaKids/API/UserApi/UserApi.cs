﻿using Proyecto26;
using System;
using API;

public class UserApi : BaseApi
{
    public void GetAuth(AuthPayload payload, Action<AuthProxy> OnSuccess = null, Action<ErrorProxy> OnError = null)
    {
        RequestHelper request = new RequestHelper
        {
            Uri = GetFormatedPath("users/auth"),
            Body = payload,
            EnableDebug = DebugMode,
            Retries = 3,
            Timeout = 15
        };

        Action<AuthProxy> response = authProxy =>
        {
            DebugColor.Log($"[Success: {authProxy.token}]", "yellow");
            OnSuccess?.Invoke(authProxy);
        };

        Action<ErrorProxy> error = error =>
        {
            DebugColor.Log($"[Error: {(int)error.statusCode} | Message: {error.message} | StatusCode: {(int)error.statusCode} | Error: {error.error} | Path: {error.path}]", "yellow");
            OnError?.Invoke(error);
        };

        Api.Post(request, response, error);
    }

    public void GetUserMe(Action<UserProxy> OnSuccess = null, Action<ErrorProxy> OnError = null)
    {
        RequestHelper request = new RequestHelper
        {
            Uri = GetFormatedPath("users/me"),
            EnableDebug = DebugMode,
            Retries = 3,
            Timeout = 15
        };

        Action<UserProxy> response = userProxy =>
        {
            DebugColor.Log($"[Success: {userProxy.inep}]", "yellow");
            OnSuccess?.Invoke(userProxy);
        };

        Action<ErrorProxy> error = error =>
        {
            DebugColor.Log($"[Error: {(int)error.statusCode} | Message: {error.message} | StatusCode: {(int)error.statusCode} | Error: {error.error} | Path: {error.path}]", "yellow");
            OnError?.Invoke(error);
        };

        Api.Get(request, response, error);
    }

    public void CreateUser(UpdateUserPayload payload, Action<UserProxy> OnSuccess = null, Action<ErrorProxy> OnError = null)
    {
        RequestHelper request = new RequestHelper
        {
            Uri = GetFormatedPath($"users/create"),
            Body = payload,
            EnableDebug = DebugMode
        };

        Action<UserProxy> response = userProxy =>
        {
            DebugColor.Log($"[Success: {userProxy.inep}]", "yellow");
            OnSuccess?.Invoke(userProxy);
        };

        Action<ErrorProxy> error = error =>
        {
            DebugColor.Log($"[Error: {(int)error.statusCode} | Message: {error.message} | StatusCode: {(int)error.statusCode} | Error: {error.error} | Path: {error.path}]", "yellow");
            OnError?.Invoke(error);
        };

        Api.Post(request, response, error);
    }
}