﻿using System;

[Serializable]
public class MedalsPayload : Payload
{
    public string name;
    public string description;
}