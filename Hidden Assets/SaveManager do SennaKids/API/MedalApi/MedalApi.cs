using Proyecto26;
using System;
using API;

public class MedalApi : BaseApi
{
    public void PostMedal(Payload payload, Action<MedalsProxy> OnComplete = null, Action<ErrorProxy> OnError = null)
    {
        MedalsPayload payloadToCloud = payload as MedalsPayload;

        PostMedal_Request(payloadToCloud,
        proxy =>
        {
            OnComplete?.Invoke(proxy);
        },
        error =>
        {
            OnError?.Invoke(error);
        });
    }

    private void PostMedal_Request(MedalsPayload payload, Action<MedalsProxy> response, Action<ErrorProxy> error)
    {
        var currentRequest = new RequestHelper
        {
            Uri = GetFormatedPath("medals/create"),
            EnableDebug = DebugMode,
            Body = payload,
            Retries = 3,
            Timeout = 15
        };

        Api.Post(currentRequest, response, error);
    }
}