using UnityEngine.Networking;
using System.Collections;
using UnityEngine;
using Proyecto26;
using System.IO;
using System;
using API;

/*
// Funcionalidades //
1. O usu�rio pode jogar Online ou Offline
2. Caso ele jogue Online, ele far� Login na API
3. Caso ele tenha um save Local e um save da API, devemos escolher um dos 2 saves para usar e substituir o outro.
4. Caso o usu�rio logue sem internet e n�o tenha um save local, ele precisa adicionar seus dados de cadastro.
5. Caso o usu�rio logue sem internet e tenha um save local, ele n precisa adicionar seus dados novamente.
6. A cada mudan�a no save no jogo, � adicionado +1 na Version do save e com isso comparamos com o Save da API e ver qual � o mais recente.


// Testes //
1. Entrar com Internet
2. Entrar sem Internet com um usu�rio que n�o exista save local
3. Entrar sem Internet com um usu�rio que exista save local
4. Ficar sem Internet dentro do jogo
5. Ficar com Internet dentro do jogo

// TODO //
1. Verificar o que acontece ao cadastrar um usuario offline, e ao tentar cadastrar ele online der algum problema com esses dados.
2. Talvez tirar o Version do save e substituir por Data e Hora do ultimo save. Ainda pode dar problema, mas o certo seria deixar a pessoa escolher qual dos saves usar.
*/

public class SaveManager : MonoBehaviour
{
    public SaveData data;

    private string beginLog = "------------------------- BEGIN -------------------------";
    private string endLog = "-------------------------- END --------------------------";

    #region Main Functions

    public static SaveManager instance;

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else if (instance != this)
        {
            Destroy(gameObject);
        }

        DontDestroyOnLoad(gameObject);

        data.SerializeData();

        OnSaveGameLoop();
    }

    #endregion

    #region OnSaveGameLoop

    // Fun��o principal de save. Ele loga e faz upload de dados de todos os usu�rios //
    public void OnSaveGameLoop(Action OnSuccess = null, Action OnError = null)
    {
        string debugColor = "cyan";

        DebugColor.Log(beginLog, debugColor);
        DebugColor.Log($"> SaveGameLoop.", debugColor);

        CheckHasInternetConnection(OnHasInternet: () =>
        {
            DebugColor.Log($">> Estou conectado na Internet.", debugColor);

            if (string.IsNullOrEmpty(data.asyncLogin.inep))
            {
                DebugColor.Log($">>> N�o h� nenhum usu�rio logado localmente. Ent�o buscarei por algum LocalSave de usu�rios para salvar na API.", debugColor);
                DebugColor.Log($">>>> Irei verificar se existe algum LocalSave de algum usuario.", debugColor);

                CheckExistOtherUserToSave(OnExist: inep =>
                {
                    DebugColor.Log($">>>>> Opa, existe sim um LocalSave. Do usu�rio: {inep}.", debugColor);
                    DebugColor.Log($">>>>>> Agora irei logar com esse usu�rio para subir esse LocalSave na API.", debugColor);

                    data.asyncLogin.LocalAsyncLogin(inep);
                    DebugColor.Log(endLog, debugColor);
                    OnSaveGameLoop(OnSuccess, OnError);
                },
                OnNotExist: () =>
                {
                    DebugColor.Log($">>>>> Procurei mas n�o existe nenhum LocalSave neste dispositivo.", debugColor);
                    DebugColor.Log($">>>>>> O SaveGameLoop Foi Concluido Com Sucesso.", debugColor);

                    LoopSuccess(OnSuccess);
                });
            }
            else
            {
                DebugColor.Log($">>> Existe um usu�rio logado localmente. Estou salvando.", debugColor);

                data.Save();

                DebugColor.Log($">>>> Iniciando o SaveGameLoop Do {data.asyncLogin.inep}.", debugColor);

                OnApiLogin(OnSuccess: () =>
                {
                    DebugColor.Log($">>>>> Logado No {data.asyncLogin.inep} Com Sucesso.", debugColor);

                    bool existSaveToCloudFile = File.Exists(data.asyncLogin.GetSaveToCloudPath());

                    if (existSaveToCloudFile)
                    {
                        DebugColor.Log($">>>>>> O Save {data.asyncLogin.inep} Existe. Vou fazer Upload do Payload.", debugColor);

                        OnUpload(OnSuccess: () =>
                        {
                            DebugColor.Log($">>>>>>> Upload Do Save Do {data.asyncLogin.inep} Feito Com Sucesso.", debugColor);

                            data.Save();

                            DeleteSaveToCloudOfLoggedUser(OnSuccess: () =>
                            {
                                DebugColor.Log($">>>>>>>> Irei verificar se existe algum LocalSave de algum usuario.", debugColor);

                                CheckExistOtherUserToSave(OnExist: inep =>
                                {
                                    DebugColor.Log($">>>>>>>>> Opa, existe sim um LocalSave. Do usu�rio: {inep}.", debugColor);
                                    DebugColor.Log($">>>>>>>>>> Agora irei logar com esse usu�rio para subir esse LocalSave na API.", debugColor);

                                    data.asyncLogin.LocalAsyncLogin(inep);
                                    DebugColor.Log(endLog, debugColor);
                                    OnSaveGameLoop(OnSuccess, OnError);
                                },
                                OnNotExist: () =>
                                {
                                    DebugColor.Log($">>>>>>>>> Procurei mas n�o existe nenhum LocalSave neste dispositivo.", debugColor);
                                    DebugColor.Log($">>>>>>>>>> O SaveGameLoop Foi Concluido Com Sucesso.", debugColor);

                                    LoopSuccess(OnSuccess);
                                });
                            },
                            OnError: () => 
                            {
                                DebugColor.Log(endLog, debugColor);
                                OnSaveGameLoop(OnSuccess, OnError);
                            });
                        },
                        OnError: () =>
                        {
                            LoopError(OnError);
                        });
                    }
                    else
                    {
                        DebugColor.Log($">>>>>> O Save Do {data.asyncLogin.inep} N�o Existe.", debugColor);
                        DebugColor.Log($">>>>>>> Irei verificar se existe algum LocalSave de algum usuario.", debugColor);

                        CheckExistOtherUserToSave(OnExist: inep =>
                        {
                            DebugColor.Log($">>>>>>> Opa, existe sim um LocalSave. Do usu�rio: {inep}.", debugColor);
                            DebugColor.Log($">>>>>>>> Agora irei logar com esse usu�rio para subir esse LocalSave na API.", debugColor);

                            data.asyncLogin.LocalAsyncLogin(inep);
                            DebugColor.Log(endLog, debugColor);
                            OnSaveGameLoop(OnSuccess, OnError);
                        },
                        OnNotExist: () =>
                        {
                            DebugColor.Log($">>>>>>> Procurei mas n�o existe nenhum LocalSave neste dispositivo.", debugColor);
                            DebugColor.Log($">>>>>>>> O SaveGameLoop Foi Concluido Com Sucesso.", debugColor);

                            LoopSuccess(OnSuccess);
                        });
                    }
                },
                OnError: error =>
                {
                    LoopError(OnError);
                });
            }
        },
        OnHasntInternet: () =>
        {
            DebugColor.Log($">> N�o estou conectado na Internet.", debugColor);
            LoopError(OnError);
        });
    }

    // Quando o SaveGameLoop � concluido //
    private void LoopSuccess(Action OnSuccess = null)
    {
        string debugColor = "blue";

        if (!string.IsNullOrEmpty(data.asyncLogin.mainInep))
        {
            DebugColor.Log($">>>>>>>>> Agora irei voltar a logar no usuario principal.", debugColor);
            DebugColor.Log(endLog, debugColor);
            OnMainLogin(data.asyncLogin.mainInep, OnSuccess, error => { OnSuccess?.Invoke(); }, error => { OnSuccess?.Invoke(); });
        }
        else
        {
            DebugColor.Log($">>>>>>>>> Como ainda n�o fiz Login Local, irei fazer Async Logout.", debugColor);

            data.asyncLogin.LocalAsyncLogout();
            DebugColor.Log(endLog, debugColor);
            OnSuccess?.Invoke();
        }
    }

    // Quando algo d� errado dentro do OnSaveGameLoop, o usu�rio loga localmente //
    private void LoopError(Action OnError)
    {
        string debugColor = "cyan";

        DebugColor.Log($"[SaveGameLoop Failed {data.asyncLogin.inep}]", "yellow");

        data.asyncLogin.LocalAsyncLogin(data.asyncLogin.inep);

        DebugColor.Log(endLog, debugColor);
        OnError?.Invoke();
    }

    // Verifica se existe algum LocalSave //
    private void CheckExistOtherUserToSave(Action<string> OnExist = null, Action OnNotExist = null)
    {
        string[] usersFolder = Directory.GetDirectories(data.GetAllUsersFolderPath());

        foreach (var userFolder in usersFolder)
        {
            if (File.Exists(data.GetSaveToCloudPathByUserPath(userFolder)))
            {
                string GetInepByUserFolder(string userFolder)
                {
                    string[] routes = userFolder.Split('\\');
                    string[] routesMobile = routes[routes.Length - 1].Split('/');
                    return routesMobile[routesMobile.Length - 1];
                }

                string inep = GetInepByUserFolder(userFolder);

                OnExist?.Invoke(inep);
                return;
            }
        }

        OnNotExist?.Invoke();
    }

    #endregion

    #region LoginRegister Local

    // Faz o login localmente do usu�rio e tenta logar na API //
    public void OnMainLogin(string inep, Action OnLoginApiSuccess = null, Action<ErrorProxy> OnErrorWithLocalLogin = null, Action<ErrorProxy> OnError = null)
    {
        if (!string.IsNullOrEmpty(data.asyncLogin.mainInep))
        {
            DebugColor.Log($"[Already Have a Main User]", "yellow");

            OnError?.Invoke(new ErrorProxy { message = "J� h� algum usu�rio logado na API", statusCode = StatusCode.InternalServerError });
        }
        else
        {
            DebugColor.Log($"[Local Login {inep}]", "yellow");

            data.asyncLogin.mainInep = inep;

            data.asyncLogin.LocalAsyncLogin(inep);

            OnApiLogin(OnLoginApiSuccess, error =>
            {
                if (data.gameData.CheckExistGameDataFile())
                {
                    OnErrorWithLocalLogin(error);
                }
                else
                {
                    OnError?.Invoke(error);
                }
            });
        }
    }

    // Faz o registro localmente do usu�rio e tenta registrar e logar na API //
    public void OnMainRegister(string inep, string name, Action OnSuccess = null, Action<ErrorProxy> OnError = null)
    {
        DebugColor.Log($"[Local Register {inep}]", "yellow");

        data.asyncLogin.LocalAsyncLogin(inep);

        data.gameData.registerUserPayload = new UpdateUserPayload() { inep = inep, name = name };

        OnApiRegister(OnSuccess, OnError);
    }

    #endregion

    #region Upload

    // Faz upload de dados do SaveToCloud para a API//
    private void OnUpload(Action OnSuccess, Action OnError)
    {
        string debugColor = "magenta";

        DebugColor.Log(beginLog, debugColor);
        DebugColor.Log($"> OnUpload.", debugColor);

        CheckHasInternetConnection(OnHasInternet: () =>
        {
            Payload payload = data.gameDataToCloud.payloads[0];

            if(payload.GetType() == typeof(MedalsPayload))
            {
                APIFactory.GetApi<MedalApi>().PostMedal(payload,
                medalProxy =>
                {
                    DebugColor.Log(">> Subi uma medalha na API.", debugColor);

                    data.gameData.userProxy.medals.Add(medalProxy);
                    data.gameDataToCloud.RemovePayload(payload);
                    DebugColor.Log(endLog, debugColor);
                    OnSuccess?.Invoke();
                },
                error =>
                {
                    DebugColor.Log(">> Erro ao subir as medalhas na API.", debugColor);
                    DebugColor.Log(endLog, debugColor);
                    OnError.Invoke();
                });
            }
            else
            {
                DebugColor.Log(">> N�o era pra dar esse erro, algo est� mal configurado.", debugColor);
                DebugColor.Log(endLog, debugColor);
                OnError.Invoke();
            }
        },
        OnHasntInternet: () =>
        {
            DebugColor.Log(endLog, debugColor);
            OnError?.Invoke();
        });
    }

    #endregion

    #region DeleteFiles

    // Deleta somente o arquivo de SaveToCloud do usu�rio atualmente logado //
    private void DeleteSaveToCloudOfLoggedUser(Action OnSuccess, Action OnError)
    {
        string savePath = data.asyncLogin.GetSaveToCloudPath();

        if (!data.gameDataToCloud.CheckHasDataToCloud() && File.Exists(savePath))
        {
            DebugColor.Log($"[GameDataToCloud File Deleted {data.asyncLogin.inep}]", "yellow");

            File.Delete(savePath);

            OnSuccess?.Invoke();
        }
        else
        {
            OnError?.Invoke();
        }
    }

    // Deleta todos os arquivos de save do usu�rio atualmente logado //
    private void DeleteAllFilesOfLoggedUser()
    {
        if (Directory.Exists(data.asyncLogin.GetUserPath()))
        {
            DebugColor.Log($"[GameData Local Save File Deleted {data.asyncLogin.inep}]", "yellow");
            DebugColor.Log($"[GameDataToCloud File Deleted {data.asyncLogin.inep}]", "yellow");

            string[] files = Directory.GetFiles(data.asyncLogin.GetUserPath());

            foreach (var filePath in files)
            {
                File.Delete(filePath);
            }
        }
    }

    public void DeleteAllFilesOfAllUsers()
    {
        string allUsersPath = data.GetAllUsersFolderPath();

        if (Directory.Exists(allUsersPath))
        {
            Directory.Delete(allUsersPath, true);
        }
    }

    #endregion

    #region LoginRegister API

    // Faz o login do usu�rio na API //
    private void OnApiLogin(Action OnSuccess, Action<ErrorProxy> OnError)
    {
        string debugColor = "green";
        DebugColor.Log(beginLog, debugColor);
        DebugColor.Log($"> OnApiLogin.", debugColor);

        if (!data.asyncLogin.isAPILogged)
        {
            DebugColor.Log($">> Vou tentar fazer o Login na API.", debugColor);

            CheckHasInternetConnection(OnHasInternet: () =>
            {
                DebugColor.Log($">>> Estou conectado na Internet.", debugColor);
                DebugColor.Log($">>> Vou tentar pegar o Token.", debugColor);

                APIFactory.GetApi<UserApi>().GetAuth(new AuthPayload()
                {
                    inep = data.asyncLogin.inep
                },
                OnSuccess: authProxy =>
                {
                    DebugColor.Log($">>>> Peguei o Token.", debugColor);
                    DebugColor.Log($">>>> Vou tentar pegar o UserMe.", debugColor);

                    RestClient.DefaultRequestHeaders["Authorization"] = "Bearer " + authProxy.token;

                    APIFactory.GetApi<UserApi>().GetUserMe(
                    OnSuccess: userProxy =>
                    {
                        DebugColor.Log($">>>>> Peguei o UserMe.", debugColor);

                        //OBS: O certo aqui seria mostrar uma modal perguntando qual dos 2 saves pegar

                        if (File.Exists(data.asyncLogin.GetGameDataSavePath()) && data.gameData.userProxy.version > userProxy.version)
                        {
                            DebugColor.Log($"[Load Local]", "yellow");

                            data.gameData = data.gameData.Load();
                            data.gameData.userProxy.id = userProxy.id;
                            data.gameData.userProxy.name = userProxy.name;
                        }
                        else
                        {
                            DebugColor.Log($"[Load API]", "yellow");
                            DebugColor.Log($">>>>>> Resetado os valores de GameDataToCloud, adicionado o UserProxy da Api e Salvo Localmente.", debugColor);

                            DeleteAllFilesOfLoggedUser();
                            data.gameDataToCloud = new GameDataToCloud();
                            data.gameData.userProxy = userProxy;
                            data.gameData.Save();
                        }

                        data.asyncLogin.isAPILogged = true;

                        DebugColor.Log($">>>>>>> Usuario logado com sucesso.", debugColor);
                        DebugColor.Log(endLog, debugColor);
                        OnSuccess?.Invoke();
                    },
                    OnError: error =>
                    {
                        DebugColor.Log($">>>>> Erro ao tentar pegar o UserMe.", debugColor);
                        DebugColor.Log(endLog, debugColor);
                        OnError?.Invoke(error);
                    });
                },
                OnError: error =>
                {
                    DebugColor.Log($">>>> Erro ao pegar o token.", debugColor);

                    if (error.statusCode == StatusCode.Unauthorized)
                    {
                        DebugColor.Log($">>>>> Nenhum usuario com esse inep encontrado.", debugColor);

                        // Existe a possibilidade do jogador tentar logar com um usuario que n exista.
                        if (!string.IsNullOrEmpty(data.gameData.registerUserPayload.name))
                        {
                            DebugColor.Log(endLog, debugColor);
                            OnApiRegister(OnSuccess, OnError);
                        }
                        else
                        {
                            DebugColor.Log($">>>>>> N�o tenho dados para cadastrar esse usu�rio", debugColor);
                            DebugColor.Log(endLog, debugColor);
                            OnError?.Invoke(error);
                        }
                    }
                    else
                    {
                        DebugColor.Log($">>>>> Erro ao tentar pegar o Token.", debugColor);
                        DebugColor.Log(endLog, debugColor);
                        OnError?.Invoke(error);
                    }
                });
            },
            OnHasntInternet: () =>
            {
                DebugColor.Log($">>> N�o estou conectado na Internet.", debugColor);
                DebugColor.Log(endLog, debugColor);
                OnError?.Invoke(new ErrorProxy { message = "N�o estou conectado na internet e n�o existe save deste usu�rio, ent�o � necess�rio informar os dados.", statusCode = StatusCode.InternalServerError });
            });
        }
        else
        {
            DebugColor.Log($">> J� estou logado na API.", debugColor);
            DebugColor.Log(endLog, debugColor);
            OnSuccess?.Invoke();
        }
    }

    private void OnApiRegister(Action OnSuccess, Action<ErrorProxy> OnError)
    {
        string debugColor = "blue";
        DebugColor.Log(beginLog, debugColor);
        DebugColor.Log($"> OnApiRegister.", debugColor);
        DebugColor.Log($">> Vou fazer o cadastro do usuario.", debugColor);

        APIFactory.GetApi<UserApi>().CreateUser(data.gameData.registerUserPayload, OnSuccess: userProxy =>
        {
            DebugColor.Log($">>> Usuario '{data.gameData.registerUserPayload.name}' criado com sucesso.", debugColor);

            data.gameData.userProxy.name = userProxy.name;
            DebugColor.Log(endLog, debugColor);
            OnSuccess?.Invoke();
        },
        OnError: error =>
        {
            DebugColor.Log(endLog, debugColor);
            OnError?.Invoke(error);
        });
    }

    #endregion

    #region Check Internet Connection

    // Verifica se existe conex�o com a internet //
    public void CheckHasInternetConnection(Action OnHasInternet = null, Action OnHasntInternet = null)
    {
        StartCoroutine(CheckHasInternetConnectionAsync(OnHasInternet, OnHasntInternet));
    }

    // Verifica se existe conex�o com a internet //
    private IEnumerator CheckHasInternetConnectionAsync(Action OnHasInternet = null, Action OnHasntInternet = null)
    {
        bool hasInternetConnection;

        using (var request = UnityWebRequest.Head("http://youtube.com"))
        {
            request.timeout = 5;
            yield return request.SendWebRequest();
            hasInternetConnection = !request.isNetworkError && !request.isHttpError && request.responseCode == 200;
        }

        if (hasInternetConnection)
        {
            OnHasInternet?.Invoke();
        }
        else
        {
            OnHasntInternet?.Invoke();
        }
    }

    #endregion
}