﻿using System.Runtime.Serialization.Formatters.Binary;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using System.IO;
using System;

[Serializable]
public class GameDataToCloud
{
    // Lista de payloads para serem salvos na API quando estiver logado e com internet
    [BoxGroup("- Payloads -")] public List<Payload> payloads = new List<Payload>();

    public void Save()
    {
        if (CheckHasDataToCloud())
        {
            var binaryFormatter = new BinaryFormatter();

            string savePath = SaveManager.instance.data.asyncLogin.GetSaveToCloudPath();

            using (var fileStream = File.Create(savePath))
            {
                binaryFormatter.Serialize(fileStream, this);
            }

            DebugColor.Log("[GameDataToCloud Save]", "yellow");
        }
        else
        {
            DebugColor.Log("[Nothing To Save On GameDataToCloud]", "yellow");
        }
    }

    public GameDataToCloud Load()
    {
        string savePath = SaveManager.instance.data.asyncLogin.GetSaveToCloudPath();

        if (File.Exists(savePath))
        {
            try
            {
                GameDataToCloud data = new GameDataToCloud();

                var binaryFormatter = new BinaryFormatter();

                using (var fileStream = File.Open(savePath, FileMode.Open))
                {
                    data = (GameDataToCloud)binaryFormatter.Deserialize(fileStream);
                }

                DebugColor.Log("[GameDataToCloud Load]", "yellow");

                return data;
            }
            catch
            {
                DebugColor.Log("[GameDataToCloud Load Error]", "yellow");

                return new GameDataToCloud();
            }
        }
        else
        {
            DebugColor.Log("[Nothing To Load On GameDataToCloud]", "yellow");

            return new GameDataToCloud();
        }
    }

    public void AddPayload(Payload payload)
    {
        payloads.Add(payload);
        SaveManager.instance.data.gameData.userProxy.version++;
        SaveManager.instance.data.Save();
    }

    public void RemovePayload(Payload payloadToRemove)
    {
        payloads.Remove(payloadToRemove);
    }

    public bool CheckHasDataToCloud()
    {
        return payloads.Count > 0;
    }

    public bool CheckExistGameDataToCloudFile()
    {
        string savePath = SaveManager.instance.data.asyncLogin.GetSaveToCloudPath();

        return File.Exists(savePath);
    }

    //SerializeData
    public GameDataToCloud()
    {
        payloads = new List<Payload>();
    }
}