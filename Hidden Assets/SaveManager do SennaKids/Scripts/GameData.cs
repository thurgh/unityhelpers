﻿using System.Runtime.Serialization.Formatters.Binary;
using Sirenix.OdinInspector;
using UnityEngine;
using System.IO;
using System;

[Serializable]
public class GameData
{
    [BoxGroup("- UserData -")] public UserProxy userProxy; // Dados do Usuário
    [BoxGroup("- UserData -")] public UpdateUserPayload registerUserPayload; // Dados do Usuário adicionados ao se registrar    

    public void Save()
    {
        var binaryFormatter = new BinaryFormatter();

        string savePath = SaveManager.instance.data.asyncLogin.GetGameDataSavePath();

        using (var fileStream = File.Create(savePath))
        {
            binaryFormatter.Serialize(fileStream, this);
        }

        DebugColor.Log("[GameData Save]", "yellow");
    }

    public GameData Load()
    {
        string savePath = SaveManager.instance.data.asyncLogin.GetGameDataSavePath();

        if (File.Exists(savePath))
        {
            try
            {
                GameData data = new GameData();

                var binaryFormatter = new BinaryFormatter();

                using (var fileStream = File.Open(savePath, FileMode.Open))
                {
                    data = (GameData)binaryFormatter.Deserialize(fileStream);
                }

                DebugColor.Log("[GameData Load]", "yellow");

                return data;
            }
            catch
            {
                DebugColor.Log("[GameData Load Error]", "yellow");

                return new GameData();
            }
        }
        else
        {
            DebugColor.Log("[Nothing To Load On GameData]", "yellow");


            return new GameData();
        }
    }

    public bool CheckExistGameDataFile()
    {
        string savePath = SaveManager.instance.data.asyncLogin.GetGameDataSavePath();

        return File.Exists(savePath);
    }

    //SerializeData
    public GameData()
    {
        userProxy = new UserProxy();
        registerUserPayload = new UpdateUserPayload();
    }
}