using Sirenix.OdinInspector;
using UnityEngine;
using System.IO;

public class SaveData : MonoBehaviour
{
    [ReadOnly] public AsyncLogin asyncLogin; // Dados do usu�rio logado
    [ReadOnly] public GameData gameData; // Todos os Dados do Jogo
    [ReadOnly] public GameDataToCloud gameDataToCloud; // Dados do Jogo para subirem na API (Esses dados nao est�o ainda no 'GameData') // Mas talvez poderia // Tipo.. deixar no GameData todo o save do jogo, mesmo o que n est� salvo. E no GameDataToCloud deixar uma lista de Payloads com tudo o que ainda precisa subir para a API

    public void Save()
    {
        gameData.Save();
        gameDataToCloud.Save();
    }

    public void Load()
    {
        gameData = gameData.Load();
        gameDataToCloud = gameDataToCloud.Load();
    }

    public string GetAllUsersFolderPath()
    {
        string allUsersFolderPath = Application.persistentDataPath + $"/Users";

        if (!Directory.Exists(allUsersFolderPath))
        {
            Directory.CreateDirectory(allUsersFolderPath);
        }

        return allUsersFolderPath;
    }

    public string GetSaveToCloudPathByUserPath(string userPath)
    {
        return $"{userPath}/GameDataSave.save";
    }

    public void SerializeData()
    {
        asyncLogin = new AsyncLogin();
        gameData = new GameData();
        gameDataToCloud = new GameDataToCloud();
    }
}