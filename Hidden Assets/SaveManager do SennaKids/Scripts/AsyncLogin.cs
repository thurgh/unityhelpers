﻿using Sirenix.OdinInspector;
using System.IO;
using System;

[Serializable]
public class AsyncLogin
{
    [BoxGroup("- SyncLogin -")] public bool isAPILogged = false;
    [BoxGroup("- SyncLogin -")] public string mainInep;
    [BoxGroup("- SyncLogin -")] public string inep;

    private string userPath;

    public void LocalAsyncLogin(string inep)
    {
        this.inep = inep;

        // Se torna false pq essa função é chamada ao trocar de usuario ou antes de logar.
        isAPILogged = false;

        userPath = $"{SaveManager.instance.data.GetAllUsersFolderPath()}/{inep}";

        if (!Directory.Exists(userPath))
        {
            Directory.CreateDirectory(userPath);
        }

        SaveManager.instance.data.Load();
    }

    public void LocalAsyncLogout()
    {
        SaveManager.instance.data.SerializeData();
    }

    public string GetUserPath()
    {
        return userPath;
    }

    public string GetSaveToCloudPath()
    {
        return $"{userPath}/GameDataSave.save";
    }

    public string GetGameDataSavePath()
    {
        return $"{userPath}/GameData.save";
    }
}