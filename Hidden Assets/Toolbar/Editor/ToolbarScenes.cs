﻿using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CreateAssetMenu(fileName = "ToolbarScenes", menuName = "ToolbarScenes")]
public class ToolbarScenes : ScriptableObject
{
	public List<SceneAsset> toolbarScenes = new List<SceneAsset>();
}
