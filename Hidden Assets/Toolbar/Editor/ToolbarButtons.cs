﻿using UnityEditor.SceneManagement;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using System.Linq;

namespace UnityToolbarExtender.Examples
{
    [InitializeOnLoad]
	public class SceneSwitchLeftButton
	{
		private static bool useNumbers;

		static SceneSwitchLeftButton()
		{
			//ToolbarExtender.LeftToolbarGUI.Add(OnToolbarGUI);
		}

		static void OnToolbarGUI()
		{
			GUILayout.FlexibleSpace();

			int margin = 8;
			float fontSize = 8.5f;

			List<SceneAsset> scenes = Resources.Load<ToolbarScenes>("ToolbarScenes").toolbarScenes;
			Font font = Resources.Load<Font>("luximb");
			List<string> scenesName = scenes.Select(x => x.name).ToList();

            for (int i = 0; i < scenesName.Count; i++)
            {
				SetButton(font, useNumbers ? i.ToString() : scenesName[i], fontSize, margin);
            }

			string toogleName = useNumbers ? "+" : "-";
			if (GUILayout.Button(new GUIContent(toogleName, toogleName), new GUIStyle("Command")
			{
				font = font,
				fontSize = 14,
				fontStyle = FontStyle.Bold,
				imagePosition = ImagePosition.ImageAbove,
				fixedWidth = (int)(toogleName.Length * fontSize) + margin
			}))
			{
				useNumbers = !useNumbers;
			}
		}

		private static void SetButton(Font font, string sceneName, float fontSize, int margin)
		{
			string sceneNameUpper = sceneName.ToUpper();

			if (GUILayout.Button(new GUIContent(sceneNameUpper, $"Open {sceneNameUpper}"), new GUIStyle("Command")
			{
				font = font,
				fontSize = 14,
				fontStyle = FontStyle.Bold,
				imagePosition = ImagePosition.ImageAbove,
				fixedWidth = (int)(sceneNameUpper.Length * fontSize) + margin
			}))
			{
				SceneHelper.StartScene(sceneNameUpper);
			}
		}
	}

	static class SceneHelper
	{
		static string sceneToOpen;

		public static void StartScene(string sceneName)
		{
			if(EditorApplication.isPlaying)
			{
				EditorApplication.isPlaying = false;
			}

			sceneToOpen = sceneName;
			EditorApplication.update += OnUpdate;
		}

		static void OnUpdate()
		{
			if (sceneToOpen == null ||
			    EditorApplication.isPlaying || EditorApplication.isPaused ||
			    EditorApplication.isCompiling || EditorApplication.isPlayingOrWillChangePlaymode)
			{
				return;
			}

			EditorApplication.update -= OnUpdate;

			if(EditorSceneManager.SaveCurrentModifiedScenesIfUserWantsTo())
			{
				// need to get scene via search because the path to the scene
				// file contains the package version so it'll change over time
				string[] guids = AssetDatabase.FindAssets("t:scene " + sceneToOpen, null);
				if (guids.Length == 0)
				{
					Debug.LogWarning("Couldn't find scene file");
				}
				else
				{
					string scenePath = AssetDatabase.GUIDToAssetPath(guids[0]);
					EditorSceneManager.OpenScene(scenePath);
					EditorApplication.isPlaying = true;
				}
			}
			sceneToOpen = null;
		}
	}
}